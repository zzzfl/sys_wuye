package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Stall;
import com.qf.wuye.service.StallService;
import com.qf.wuye.service.impl.StallServiceImpl;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * 时间：2021/4/28 16:45
 * 我无敌，你随意
 */
@WebServlet("/stall/*")
@MultipartConfig
public class StallServlet extends BaseServlet{

   public DataResult all(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
       String page = request.getParameter("page");
       String limit = request.getParameter("limit");
       String stall_number = request.getParameter("stall_number");
       StallService stallService = new StallServiceImpl();
       DataResult d = stallService.limit(page, limit, stall_number);
       return d;
   }

   public DataResult add(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
       String info = request.getParameter("info");
       ObjectMapper mapper = new ObjectMapper();
       Stall stall = mapper.readValue(info, Stall.class);
       StallService stallService = new StallServiceImpl();
       int count = stallService.addStall(stall);
       DataResult dataResult;
       if (count > 0) {
           dataResult = new DataResult(0,"添加成功");
       }else {
           dataResult = new DataResult(40000,"添加失败");
       }
        return dataResult;
   }

   public DataResult del(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
       String id = request.getParameter("stallno");
       StallService stallService = new StallServiceImpl();
       int count = stallService.deleteStall(id);
       DataResult dataResult;
       if (count > 0 ){
           dataResult = new DataResult(0,"删除成功");
       }else {
           dataResult = new DataResult(40000,"删除失败");
       }
       return dataResult;
   }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] StallIdList = mapper.readValue(ids, String[].class);
        StallService stallService = new StallServiceImpl();
        int count = stallService.deleteAllStall(StallIdList);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"成功删除"+count+"条数据");
        }else{
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String id = request.getParameter("stallno");
        StallService stallService = new StallServiceImpl();
        Stall stall = stallService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功", stall);
        return dataResult;

    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Stall stall = mapper.readValue(info, Stall.class);
        StallService stallService = new StallServiceImpl();
        int count = stallService.updateStall(stall);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }

}
