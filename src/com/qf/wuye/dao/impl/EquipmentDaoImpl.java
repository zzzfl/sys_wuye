package com.qf.wuye.dao.impl;

import com.alibaba.druid.sql.ast.statement.SQLForeignKeyImpl;
import com.qf.wuye.dao.EquipmentDao;
import com.qf.wuye.entity.Equipment;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 18:01
 * 我无敌，你随意
 */
public class EquipmentDaoImpl implements EquipmentDao {
    @Override
    public List<Equipment> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from equipment";
        return qr.query(sql,new BeanListHandler<Equipment>(Equipment.class));
    }

    @Override
    public Equipment selectOne(int equipment_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from equipment where equipment_id = ?";
        Object[] args = {equipment_id};
        return qr.query(sql,new BeanHandler<Equipment>(Equipment.class),args);
    }

    @Override
    public int addEquipment(Equipment equipment) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into equipment values (?,?,?,?,?,?,?,?,?,?)";
        Object[] args = {equipment.getEquipment_id(),equipment.getCno(),equipment.getEquipment_number(),
        equipment.getEquipment_name(),equipment.getBrand(),equipment.getPrice(),equipment.getBuy_sum(),
        equipment.getBuy_time(),equipment.getUse_time(),equipment.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int delEquipment(int equipment_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from equipment where equipment_id = ?";
        Object[] args = {equipment_id};
        return qr.update(sql,args);
    }

    @Override
    public int updateEquipment(Equipment equipment) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update equipment set cno=?,equipment_number=?,equipment_name=?,brand=?,price=?,buy_sum=?,buy_time=?,use_time=?,date=? where equipment_id=?";
        Object[] args = {equipment.getCno(),equipment.getEquipment_number(),
                equipment.getEquipment_name(),equipment.getBrand(),equipment.getPrice(),equipment.getBuy_sum(),
                equipment.getBuy_time(),equipment.getUse_time(),equipment.getDate(),equipment.getEquipment_id(),};
        return qr.update(sql,args);
    }

    @Override
    public List<Equipment> limit(Integer currentPage, Integer pageSize, String brand) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (brand == null){
            String sql = "select * from equipment limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Equipment>(Equipment.class),args);
        }else {
            String sql = "select * from equipment where brand like ? limit ?,?";
            Object[] args = {"%"+brand+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Equipment>(Equipment.class),args);
        }
    }

    @Override
    public int count(String brand) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (brand == null){
            String sql = "select count(*) from equipment";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from equipment where brand like ?";
            Object[] args = {"%"+brand+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
