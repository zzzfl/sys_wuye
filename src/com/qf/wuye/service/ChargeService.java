package com.qf.wuye.service;

import com.qf.wuye.entity.Charge;
import com.qf.wuye.entity.DataResult;

import java.util.List;

/**
 * 时间：2021/6/24 23:37
 * 我无敌，你随意
 */
public interface ChargeService {
    List<Charge> selectAll();
    Charge selectOne(String charge_id);
    List<Charge> selectSome(String cno);
    int addCharge(Charge charge);
    int delCharge(String charge_id);
    int delAll(String[] charge_ids);
    int updateCharge(Charge charge);
    DataResult limit(String currentPage, String pageSize, String charge_name);
}
