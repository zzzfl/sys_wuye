package com.qf.wuye.dao.impl;

import com.alibaba.druid.stat.JdbcSqlStatValue;
import com.qf.wuye.dao.ActivityDao;
import com.qf.wuye.entity.Activity;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 9:31
 * 我无敌，你随意
 */
public class ActivityDaoImpl implements ActivityDao {
    @Override
    public List<Activity> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from activity";
        return qr.query(sql,new BeanListHandler<Activity>(Activity.class));
    }

    @Override
    public Activity selectOne(int activity_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from activity where activity_id = ?";
        Object[] args = {activity_id};
        return qr.query(sql,new BeanHandler<Activity>(Activity.class),args);
    }

    @Override
    public int addActivity(Activity activity) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into activity values (?,?,?,?,?,?,?,?)";
        Object[] args = {activity.getActivity_id(),activity.getCno(),activity.getActivity_name(),activity.getPlace(),
        activity.getSponsor(),activity.getStartime(),activity.getEndtime(),activity.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int deleteActivity(int activity_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from activity where activity_id = ?";
        Object[] args = {activity_id};
        return qr.update(sql,args);
    }

    @Override
    public int updateActivity(Activity activity) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update activity set cno=?,activity_name=?,place=?,sponsor=?,startime=?,endtime=?,date=? where activity_id=?";
        Object[] args = {activity.getCno(),activity.getActivity_name(),activity.getPlace(),
                activity.getSponsor(),activity.getStartime(),activity.getEndtime(),activity.getDate(),activity.getActivity_id()};
        return qr.update(sql,args);
    }

    @Override
    public List<Activity> limit(Integer currentPage, Integer pageSize, String sponsor) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if(sponsor == null){
            String sql = "select * from activity limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Activity>(Activity.class),args);
        }else {
            String sql = "select * from activity where sponsor like ? limit ?,?";
            Object[] args = {"%"+sponsor+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Activity>(Activity.class),args);
        }
    }

    @Override
    public int count(String sponsor) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (sponsor == null){
            String sql = "select count(*) from activity";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from acivity where sponsor like ?";
            Object[] args = {"%"+sponsor+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
