package com.qf.wuye.dao;

import com.qf.wuye.entity.Activity;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 9:26
 * 我无敌，你随意
 */
public interface ActivityDao {
    List<Activity> selectAll() throws SQLException;
    Activity selectOne(int activity_id) throws SQLException;
    int addActivity(Activity activity) throws SQLException;
    int deleteActivity(int activity_id) throws SQLException;
    int updateActivity(Activity activity) throws SQLException;

    List<Activity> limit(Integer currentPage, Integer pageSize, String sponsor) throws SQLException;
    int count(String sponsor) throws SQLException;

}
