package com.qf.wuye.dao;

import com.qf.wuye.entity.Charge;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 20:58
 * 我无敌，你随意
 */
public interface ChargeDao {
    List<Charge> selectAll() throws SQLException;
    Charge selectOne(int charge_id) throws SQLException;
    List<Charge> selectSome(int cno) throws SQLException;
    int addCharge(Charge charge) throws SQLException;
    int delCharge(int charge_id) throws SQLException;
    int updateCharge(Charge charge) throws SQLException;
    List<Charge> limit(Integer currentPage, Integer pageSize, String charge_name) throws SQLException;
    int count(String charge_name) throws SQLException;
}
