package com.qf.wuye.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 时间：2021/6/22 17:56
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private Integer id;
    private String username;
    private String password;
    private String phone;
    private String email;
    private String position;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date start_time;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
