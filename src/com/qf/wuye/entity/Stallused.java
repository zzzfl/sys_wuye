package com.qf.wuye.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.poi.hssf.record.RecalcIdRecord;

import javax.xml.bind.PrintConversionEvent;
import java.util.Date;

/**
 * 时间：2021/6/23 14:44
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Stallused {
    private Integer stallused_id;
    private Integer cno;
    private String stall_number;
    private String car_number;
    private String image;
    private Integer pno;
    private String phone;
    private Integer price;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date startime;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date endtime;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date date;
    @JsonIgnore
    private String file;
}
