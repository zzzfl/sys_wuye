package com.qf.wuye.service.impl;

import com.qf.wuye.dao.EstateDao;
import com.qf.wuye.dao.impl.EstateDaoImpl;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Estate;
import com.qf.wuye.service.EstateService;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/26 17:23
 * 我无敌，你随意
 */
public class EstateServiceImpl implements EstateService {
    EstateDao estateDao = new EstateDaoImpl();
    @Override
    public List<Estate> selectAll() {
        try {
            return estateDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Estate selectOne(String eno) {
        try {
            int eNo = Integer.parseInt(eno);
            return estateDao.selectOne(eNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int deleteAllEstate(String[] estates) {
        for (String estate : estates) {
            int count = deleteEstate(estate);
        }
        return estates.length;
    }

    @Override
    public int addEstate(Estate estate) {
        try {
            return estateDao.addEstate(estate);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteEstate(String eno) {
        try {
            int eNo = Integer.parseInt(eno);
            return estateDao.deleteEstate(eNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int updateEstate(Estate estate) {
        try {
            return estateDao.updateEstate(estate);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String landlord) {
        try {
            int p = 1;//当前页
            int s = 5;//每页条数
            if (currentPage != null){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<Estate> estateList = estateDao.limit(p,s,landlord);
            //因为现在使用的是layui的表格渲染，所以返回到前端的数据使用要求的 1、要求code中0表示成功     2、如果开启分页那么必须要返回一个count的参数，表示总条数
            DataResult dataResult = new DataResult(0,"查询成功",estateList,estateDao.count(landlord));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
