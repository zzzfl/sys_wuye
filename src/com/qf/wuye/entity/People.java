package com.qf.wuye.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 时间：2021/4/25 17:46
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class People {
    private Integer pno;
    private Integer cno;        //所属小区
    private Integer bno;      //所属栋
    private Integer door_number;
    private String p_name;      //户主名字
    private String image;         //户主照片
    private String id_card;         //户主身份证
    private String phone;           //户主电话
    private String professional;    //户主职业
    private String sex;        //户主性别
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date date;        //户主入住日期
    @JsonIgnore
    private String file;
}
