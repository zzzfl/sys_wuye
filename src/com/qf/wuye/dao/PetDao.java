package com.qf.wuye.dao;

import com.qf.wuye.entity.Pet;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/28 15:20
 * 我无敌，你随意
 */
public interface PetDao {
    List<Pet> selectAll() throws SQLException;
    Pet selectOne(int petno) throws SQLException;
    int addPet(Pet pet) throws SQLException;
    int deletPet(int petno) throws SQLException;
    int updatePet(Pet pet) throws SQLException;

    List<Pet> limit(Integer currentPage, Integer pageSize, String pet_name) throws SQLException;
    int count(String pet_name) throws SQLException;

}
