package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Estate;
import com.qf.wuye.service.EstateService;
import com.qf.wuye.service.impl.EstateServiceImpl;
import com.qf.wuye.util.FileUploadUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 时间：2021/4/26 17:32
 * 我无敌，你随意
 */

@WebServlet("/estate/*")
@MultipartConfig
public class EstateServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String landlord = request.getParameter("landlord");
        EstateService estateService = new EstateServiceImpl();
        DataResult dataResult = estateService.limit(page,limit,landlord);
        return dataResult;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //获取请求参数
        String info = request.getParameter("info");
        //将json字符串转换成java对象
        ObjectMapper mapper = new ObjectMapper();
        Estate estate = mapper.readValue(info,Estate.class);
        //调用service方法
        EstateService estateService = new EstateServiceImpl();
        int count = estateService.addEstate(estate);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("eno");
        EstateService estateService = new EstateServiceImpl();
        int count = estateService.deleteEstate(id);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("eno");
        EstateService estateService = new EstateServiceImpl();
        Estate estate = estateService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功",estate);
        return dataResult;
    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Estate estate = mapper.readValue(info, Estate.class);
        EstateService estateService = new EstateServiceImpl();
        int count = estateService.updateEstate(estate);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult upload(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DataResult dataResult;
        try {
            String sqlPath = FileUploadUtil.upload(request);
            dataResult = new DataResult(0,"上传成功",sqlPath);
        } catch (Exception e) {
            e.printStackTrace();
            dataResult = new DataResult(40000,"上传失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] idArr = mapper.readValue(ids, String[].class);

        EstateService estateService = new EstateServiceImpl();
        int count = estateService.deleteAllEstate(idArr);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"删除"+count+"条数据成功");
        }else{
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }


}
