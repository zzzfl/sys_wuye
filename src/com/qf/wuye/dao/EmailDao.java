package com.qf.wuye.dao;

import com.qf.wuye.entity.Email;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 16:27
 * 我无敌，你随意
 */
public interface EmailDao {
    List<Email> selectAll() throws SQLException;
    Email selectOne(int email_id) throws SQLException;
    int addEmail(Email email) throws SQLException;
    int delEmail(int email_id) throws SQLException;
    int updateEmail(Email email) throws SQLException;
    List<Email> limit(Integer currentPage, Integer pageSize, String email_name) throws SQLException;
    int count(String email_name) throws SQLException;
}
