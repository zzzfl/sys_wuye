package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.DataResult;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * 时间：2021/4/23 17:00
 * 我无敌，你随意
 */
public class BaseServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("application/json;charset=utf-8");
        //获取请求参数
        String methodName = req.getParameter("method");
        if (methodName == null || methodName == ""){
            DataResult dataResult = new DataResult(40000,"请求参数method错误");
            ObjectMapper mapper = new ObjectMapper();
            resp.getWriter().write(mapper.writeValueAsString(dataResult));
        }
        //利用反射调用方法
        Class c = this.getClass();
        try {
            Method method = c.getMethod(methodName,HttpServletRequest.class,HttpServletResponse.class);
            Object result = method.invoke(c.newInstance(),req,resp);
            if (result != null){
                ObjectMapper mapper = new ObjectMapper();
                resp.getWriter().write(mapper.writeValueAsString(result));
            }
        } catch (Exception e) {
            e.printStackTrace();
            DataResult dataResult = new DataResult(40000,"请求错误");
            ObjectMapper mapper = new ObjectMapper();
            resp.getWriter().write(mapper.writeValueAsString(dataResult));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
