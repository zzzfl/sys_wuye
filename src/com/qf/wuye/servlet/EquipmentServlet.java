package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Email;
import com.qf.wuye.entity.Equipment;
import com.qf.wuye.service.EmailService;
import com.qf.wuye.service.EquipmentService;
import com.qf.wuye.service.impl.EmailServiceImpl;
import com.qf.wuye.service.impl.EquipmentServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * 时间：2021/6/24 15:39
 * 我无敌，你随意
 */
@WebServlet("/equipment/*")
public class EquipmentServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String brand = request.getParameter("brand");
        EquipmentService equipmentService = new EquipmentServiceImpl();
        DataResult dataResult = equipmentService.limit(page, limit, brand);
        return dataResult;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Equipment equipment = mapper.readValue(info, Equipment.class);
        EquipmentService equipmentService = new EquipmentServiceImpl();
        int count = equipmentService.addEquipment(equipment);
        DataResult dataResult ;
        if (count > 0){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String equipment_id = request.getParameter("equipment_id");
        EquipmentService equipmentService = new EquipmentServiceImpl();
        int count = equipmentService.delEquipment(equipment_id);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] strings = mapper.readValue(ids, String[].class);
        EquipmentService equipmentService = new EquipmentServiceImpl();
        int count = equipmentService.delAll(strings);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"成功删除"+count+"条数据");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String id = request.getParameter("equipment_id");
        EquipmentService equipmentService = new EquipmentServiceImpl();
        Equipment equipment = equipmentService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功",equipment);
        return dataResult;
    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Equipment equipment = mapper.readValue(info, Equipment.class);
        EquipmentService equipmentService = new EquipmentServiceImpl();
        int count = equipmentService.updateEquipment(equipment);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }


}
