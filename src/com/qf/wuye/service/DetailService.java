package com.qf.wuye.service;

import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Detail;

import javax.xml.crypto.Data;
import java.util.List;

/**
 * 时间：2021/6/25 9:31
 * 我无敌，你随意
 */
public interface DetailService {
    List<Detail> selectAll();
    Detail selectOne(String detail_id);
    int addDetail(Detail detail);
    int delDetail(String detail_id);
    int delAll(String[] detail_ids);
    int updateDetail(Detail detail);
    DataResult limit(String currentPage, String pageSize, String brief);
}
