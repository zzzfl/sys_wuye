package com.qf.wuye.dao.impl;

import com.qf.wuye.dao.WarrantyDao;
import com.qf.wuye.entity.Warranty;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 14:08
 * 我无敌，你随意
 */
public class WarrantyDaoImpl implements WarrantyDao {
    @Override
    public List<Warranty> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from warranty";
        return qr.query(sql,new BeanListHandler<Warranty>(Warranty.class));
    }

    @Override
    public Warranty selectOne(int warranty_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from warranty where warranty_id = ?";
        Object[] args = {warranty_id};
        return qr.query(sql, new BeanHandler<Warranty>(Warranty.class),args);
    }

    @Override
    public int addWarranty(Warranty warranty) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into warranty values (?,?,?,?,?,?)";
        Object[] args = {warranty.getWarranty_id(),warranty.getCno(),warranty.getPno(),warranty.getWarranty_name(),
                warranty.getBrief(),warranty.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int deleteWarranty(int warranty_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from warranty where warranty_id = ?";
        Object[] args = {warranty_id};
        return qr.update(sql,args);
    }

    @Override
    public int updateWarranty(Warranty warranty) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update warranty set cno=?,pno=?,warranty_name=?,brief=?,date=? where warranty_id=?";
        Object[] args = {warranty.getCno(),warranty.getPno(),warranty.getWarranty_name(),
                warranty.getBrief(),warranty.getDate(),warranty.getWarranty_id()};
        return qr.update(sql,args);
    }

    @Override
    public List<Warranty> limit(Integer currentPage, Integer pageSize, String brief) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (brief == null){
            String sql = "select * from warranty limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Warranty>(Warranty.class),args);
        }else {
            String sql = "select * from warranty where brief like ? limit ?,?";
            Object[] args = {"%"+brief+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Warranty>(Warranty.class),args);
        }
    }

    @Override
    public int count(String brief) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (brief == null ){
            String sql = "select count(*) from warranty";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from warranty where brief like ?";
            Object[] args = {"%"+brief+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
