package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.dao.DetailDao;
import com.qf.wuye.entity.Activity;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Detail;
import com.qf.wuye.service.ActivityService;
import com.qf.wuye.service.DetailService;
import com.qf.wuye.service.impl.ActivityServiceImpl;
import com.qf.wuye.service.impl.DetailServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * 时间：2021/6/24 10:03
 * 我无敌，你随意
 */
@WebServlet("/detail/*")
public class DetailServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String brief = request.getParameter("brief");
        DetailService detailService = new DetailServiceImpl();
        DataResult d = detailService.limit(page, limit, brief);
        return d;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Detail detail = mapper.readValue(info, Detail.class);
        DetailService detailService = new DetailServiceImpl();
        int count = detailService.addDetail(detail);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String id = request.getParameter("detail_id");
        DetailService detailService = new DetailServiceImpl();
        int count = detailService.delDetail(id);
        DataResult dataResult ;
        if (count > 0){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] detailList = mapper.readValue(ids, String[].class);
        DetailService detailService = new DetailServiceImpl();
        int count = detailService.delAll(detailList);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"成功删除"+count+"条数据");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String id = request.getParameter("detail_id");
        DetailService detailService = new DetailServiceImpl();
        Detail detail = detailService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功",detail);
        return dataResult;

    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Detail detail = mapper.readValue(info, Detail.class);
        DetailService detailService = new DetailServiceImpl();
        int count = detailService.updateDetail(detail);
        DataResult dataResult ;
        if (count > 0){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }

}
