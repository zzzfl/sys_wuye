package com.qf.wuye.service.impl;

import com.qf.wuye.dao.StallusedDao;
import com.qf.wuye.dao.impl.StallusedDaoImpl;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Stallused;
import com.qf.wuye.service.StallusedService;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/23 15:43
 * 我无敌，你随意
 */
public class StallusedServiceImpl implements StallusedService {
    StallusedDao stallusedDao = new StallusedDaoImpl();
    @Override
    public List<Stallused> selectAll() {
        try {
            return stallusedDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Stallused selectOne(String stallused_id) {
        int id = Integer.parseInt(stallused_id);
        try {
            return stallusedDao.selectOne(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addStallused(Stallused stallused) {
        try {
            return stallusedDao.addStallused(stallused);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteStallused(String stallused_id) {
        int id = Integer.parseInt(stallused_id);
        try {
            return stallusedDao.deleteStallused(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteAllStallused(String[] stalluseds) {
        for (String stallused :stalluseds ) {
            int count =deleteStallused(stallused);
        }
        return stalluseds.length;
    }

    @Override
    public int updateStallused(Stallused stallused) {
        try {
            return stallusedDao.updateStallused(stallused);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String stall_number) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null ){
                s = Integer.parseInt(pageSize);
            }
            List<Stallused> stallusedList = stallusedDao.limit(p, s, stall_number);
            DataResult dataResult = new DataResult(0,"查询成功",stallusedList,stallusedDao.count(stall_number));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
