package com.qf.wuye.dao.impl;

import com.qf.wuye.dao.CarDao;
import com.qf.wuye.entity.Car;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/28 12:08
 * 我无敌，你随意
 */
public class CarDaoImpl implements CarDao {
    @Override
    public List<Car> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from car";
        return qr.query(sql,new BeanListHandler<Car>(Car.class));
    }

    @Override
    public Car selectOne(int carno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from car where carno = ?";
        Object[] args = {carno};
        return qr.query(sql, new BeanHandler<Car>(Car.class),args);
    }

    @Override
    public int addCar(Car car) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into car values (?,?,?,?,?,?)";
        Object[] args = {car.getCarno(),car.getImage(),car.getCar_number(),car.getPno(),car.getColor(),car.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int deletCar(int carno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from car where carno = ?";
        Object[] args = {carno};
        return qr.update(sql,args);
    }

    @Override
    public int updateCar(Car car) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update car set image=?,car_number=?,pno=?,color=?,date=? where carno=?";
        Object[] args = {car.getImage(),car.getCar_number(),car.getPno(),car.getColor(),car.getDate(),car.getCarno()};
        return qr.update(sql,args);
    }

    @Override
    public List<Car> limit(Integer currentPage, Integer pageSize, String car_number) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (car_number == null){
            String sql = "select * from car limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Car>(Car.class),args);
        }else {
            String sql = "select * from car where car_number like ? limit ?,?";
            Object[] args = {"%"+car_number+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Car>(Car.class),args);
        }
    }

    @Override
    public int count(String car_number) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (car_number == null){
            String sql = "select count(*) from car";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from car where car_number like ?";
            Object[] args = {"%"+car_number+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
