package com.qf.wuye.dao.impl;

import com.qf.wuye.dao.PeopleDao;
import com.qf.wuye.entity.People;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/28 9:24
 * 我无敌，你随意
 */
public class PeopleDaoImpl implements PeopleDao {
    @Override
    public List<People> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from people";
        return qr.query(sql,new BeanListHandler<People>(People.class));
    }

    @Override
    public People selectOne(int pno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from people where pno = ?";
        Object[] args = {pno};
        return qr.query(sql,new BeanHandler<People>(People.class),args);
    }

    @Override
    public List<People> selectSome(int cno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from people where cno = ?";
        Object[] args = {cno};
        return qr.query(sql, new BeanListHandler<People>(People.class),args);
    }

    @Override
    public int addPeople(People people) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into people values (?,?,?,?,?,?,?,?,?,?,?)";
        Object[] args = {people.getPno(),people.getCno(),people.getBno(),people.getDoor_number(),people.getP_name(),people.getImage(),people.getId_card(),people.getPhone(),people.getProfessional(),people.getSex(),people.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int deletePeople(int pno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from people where pno = ?";
        Object[] args = {pno};
        return qr.update(sql,args);
    }

    @Override
    public int updatePeople(People people) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update people set cno=?,bno=?,door_number=?,p_name=?,image=?,id_card=?,phone=?,professional=?,sex=?,date=? where pno=?";
        Object[] args = {people.getCno(),people.getBno(),people.getDoor_number(),people.getP_name(),people.getImage(),people.getId_card(),people.getPhone(),people.getProfessional(),people.getSex(),people.getDate(),people.getPno()};
        return qr.update(sql,args);
    }

    @Override
    public List<People> limit(Integer currentPage, Integer pageSize, String p_name) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (p_name == null){
            String sql = "select * from people limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<People>(People.class),args);
        }else {
            String sql = "select * from people where p_name like ? limit ?,?";
            Object[] args = {"%"+p_name+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<People>(People.class),args);
        }
    }

    @Override
    public int count(String p_name) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (p_name == null){
            String sql = "select count(*) from people";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from people where p_name like ?";
            Object[] args = {"%"+p_name+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
