package com.qf.wuye.service;

import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.User;

import java.util.List;

/**
 * 时间：2021/6/22 19:15
 * 我无敌，你随意
 */
public interface UserService {
    boolean selectUserByName(User user);
    List<User> selectAll();
    User selectOne(String id);
    int addUser(User user);
    int delUser(String id);
    int delAll(String[] ids);
    int updateUser(User user);
    DataResult limit(String currentPage, String pageSize, String username);
}
