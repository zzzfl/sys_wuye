package com.qf.wuye.service.impl;

import com.qf.wuye.dao.ActivityDao;
import com.qf.wuye.dao.impl.ActivityDaoImpl;
import com.qf.wuye.entity.Activity;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.service.ActivityService;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 9:53
 * 我无敌，你随意
 */
public class ActivityServiceImpl implements ActivityService {
    ActivityDao activityDao = new ActivityDaoImpl();

    @Override
    public List<Activity> selectAll() {
        try {
            return activityDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Activity selectOne(String activity_id) {
        int id = Integer.parseInt(activity_id);
        try {
            return activityDao.selectOne(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addActivity(Activity activity) {
        try {
            return activityDao.addActivity(activity);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteActivity(String activity_id) {
        int id = Integer.parseInt(activity_id);
        try {
            return activityDao.deleteActivity(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteAll(String[] activity_ids) {
        for (String id : activity_ids  ) {
            int count = deleteActivity(id);
        }
        return activity_ids.length;
    }

    @Override
    public int updateActivity(Activity activity) {
        try {
            return activityDao.updateActivity(activity);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String sponsor) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<Activity> activityList = activityDao.limit(p, s, sponsor);
            DataResult dataResult = new DataResult(0,"查询成功",activityList,activityDao.count(sponsor));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
