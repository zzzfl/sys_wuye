package com.qf.wuye.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 时间：2021/4/20 19:44
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataResult<T> {
    private Integer code; //10000 20000
    private String msg;
    private T data;
    private Integer count;

    public DataResult(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public DataResult(Integer code , String msg, T t){
        this.code = code;
        this.msg = msg;
        this.data = t;
    }
}
