package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.Complaint;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.service.ComplaintService;
import com.qf.wuye.service.impl.ComplaintServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * 时间：2021/6/24 15:39
 * 我无敌，你随意
 */
@WebServlet("/complaint/*")
public class ComplaintServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String thing = request.getParameter("thing");
        ComplaintService complaintService = new ComplaintServiceImpl();
        DataResult dataResult = complaintService.limit(page, limit, thing);
        return dataResult;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Complaint complaint = mapper.readValue(info, Complaint.class);
        ComplaintService complaintService = new ComplaintServiceImpl();
        int count = complaintService.addComplaint(complaint);
        DataResult dataResult ;
        if (count > 0){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String complaint_id = request.getParameter("complaint_id");
        ComplaintService complaintService = new ComplaintServiceImpl();
        int count = complaintService.delComplaint(complaint_id);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] strings = mapper.readValue(ids, String[].class);
        ComplaintService complaintService = new ComplaintServiceImpl();
        int count = complaintService.delAll(strings);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"成功删除"+count+"条数据");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String id = request.getParameter("complaint_id");
        ComplaintService complaintService = new ComplaintServiceImpl();
        Complaint complaint = complaintService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功",complaint);
        return dataResult;
    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Complaint complaint = mapper.readValue(info, Complaint.class);
        ComplaintService complaintService = new ComplaintServiceImpl();
        int count = complaintService.updateComplaint(complaint);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }


}
