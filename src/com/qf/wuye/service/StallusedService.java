package com.qf.wuye.service;

import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Stall;
import com.qf.wuye.entity.Stallused;

import java.util.List;

/**
 * 时间：2021/6/23 15:28
 * 我无敌，你随意
 */
public interface StallusedService {
    List<Stallused> selectAll();
    Stallused selectOne(String stallused_id);
    int addStallused(Stallused stallused);
    int deleteStallused(String stallused_id);
    int deleteAllStallused(String[] stalluseds);
    int updateStallused(Stallused stallused);

    DataResult limit(String currentPage, String pageSize,String stall_number);

}
