package com.qf.wuye.service.impl;

import com.qf.wuye.dao.EmailDao;
import com.qf.wuye.dao.impl.EmailDaoImpl;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Email;
import com.qf.wuye.service.EmailService;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 16:47
 * 我无敌，你随意
 */
public class EmailServiceImpl implements EmailService {
    EmailDao emailDao = new EmailDaoImpl();

    @Override
    public List<Email> selectAll() {
        try {
            return emailDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Email selectOne(String email_id) {
        int id = Integer.parseInt(email_id);
        try {
            return emailDao.selectOne(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addEmail(Email email) {
        try {
            return emailDao.addEmail(email);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delEmail(String email_id) {
        int id = Integer.parseInt(email_id);
        try {
            return emailDao.delEmail(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delAll(String[] email_ids) {
        for ( String id : email_ids ) {
            int count = delEmail(id);
        }
        return email_ids.length;
    }

    @Override
    public int updateEmail(Email email) {
        try {
            return emailDao.updateEmail(email);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String email_name) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<Email> emailList = emailDao.limit(p, s, email_name);
            DataResult dataResult = new DataResult(0,"查询成功",emailList,emailDao.count(email_name));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
