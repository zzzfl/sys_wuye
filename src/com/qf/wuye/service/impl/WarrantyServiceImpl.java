package com.qf.wuye.service.impl;

import com.qf.wuye.dao.WarrantyDao;
import com.qf.wuye.dao.impl.WarrantyDaoImpl;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Warranty;
import com.qf.wuye.service.WarrantyService;

import java.sql.SQLException;
import java.util.List;
import java.util.zip.CRC32;

/**
 * 时间：2021/6/24 14:22
 * 我无敌，你随意
 */
public class WarrantyServiceImpl implements WarrantyService {
    WarrantyDao warrantyDao = new WarrantyDaoImpl();

    @Override
    public List<Warranty> selectAll() {
        try {
            return warrantyDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Warranty selectOne(String warranty_id) {
        try {
            int id = Integer.parseInt(warranty_id);
            return warrantyDao.selectOne(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addWarranty(Warranty warranty) {
        try {
            return warrantyDao.addWarranty(warranty);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delWarranty(String warranty_id) {
        int id = Integer.parseInt(warranty_id);
        try {
            return warrantyDao.deleteWarranty(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delAll(String[] warranty_ids) {
        for ( String id : warranty_ids     ) {
            int count = delWarranty(id);
        }
        return warranty_ids.length;
    }

    @Override
    public int updateWarranty(Warranty warranty) {
        try {
            return warrantyDao.updateWarranty(warranty);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String brief) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<Warranty> warrantyList = warrantyDao.limit(p, s, brief);
            DataResult dataResult = new DataResult(0,"查询成功",warrantyList, warrantyDao.count(brief));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
