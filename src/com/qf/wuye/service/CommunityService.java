package com.qf.wuye.service;

import com.qf.wuye.entity.Community;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Community;
import com.qf.wuye.entity.DataResult;

import java.util.List;

/**
 * 时间：2021/4/26 9:18
 * 我无敌，你随意
 */
public interface CommunityService {
    List<Community> selectAll();
    Community selectOne(String cno);
    int addCommunity(Community community);
    int deleteCommunity(String cno);
    int deleteAllCommunity(String[] communities);
    int updateCommunity(Community community);

    //导入数据
    int addAllCommunity(List<String[]> data);

    DataResult limit(String currentPage , String pageSize, String addr);

}
