package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Pet;
import com.qf.wuye.service.PetService;
import com.qf.wuye.service.impl.PetServiceImpl;
import com.qf.wuye.util.FileUploadUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * 时间：2021/4/28 15:36
 * 我无敌，你随意
 */
@WebServlet("/pet/*")
@MultipartConfig
public class PetServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String pet_name = request.getParameter("pet_name");
        PetService petService = new PetServiceImpl();
        DataResult dataResult = petService.limit(page, limit, pet_name);
        return dataResult;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Pet pet = mapper.readValue(info, Pet.class);
        PetService petService = new PetServiceImpl();
        int count = petService.addPet(pet);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String id = request.getParameter("petno");
        PetService petService = new PetServiceImpl();
        int count = petService.deletePet(id);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] petIdList = mapper.readValue(ids, String[].class);
        PetService petService = new PetServiceImpl();
        int count = petService.deleteAllPet(petIdList);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"成功删除"+count+"条数据");
        }else{
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String id = request.getParameter("petno");
        PetService petService = new PetServiceImpl();
        Pet pet = petService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功", pet);
        return dataResult;

    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Pet pet = mapper.readValue(info, Pet.class);
        PetService petService = new PetServiceImpl();
        int count = petService.updatePet(pet);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }

    public DataResult upload(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        DataResult dataResult;
        try {
            String sqlPath = FileUploadUtil.upload(request);
            dataResult = new DataResult(0,"上传成功",sqlPath);
            return dataResult;
        } catch (ServletException e) {
            e.printStackTrace();
            dataResult = new DataResult(40000,"上传失败");
        }
        return dataResult;
    }


}
