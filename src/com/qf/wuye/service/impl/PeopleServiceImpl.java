package com.qf.wuye.service.impl;

import com.qf.wuye.dao.PeopleDao;
import com.qf.wuye.dao.impl.PeopleDaoImpl;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.People;
import com.qf.wuye.service.PeopleService;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/28 9:51
 * 我无敌，你随意
 */
public class PeopleServiceImpl implements PeopleService {
    PeopleDao peopleDao = new PeopleDaoImpl();
    @Override
    public List<People> selectAll() {
        try {
            return peopleDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public People selectOne(String pno) {
        try {
            int iNo = Integer.parseInt(pno);
            return peopleDao.selectOne(iNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public List<People> selectSome(String cno) {
        int id = Integer.parseInt(cno);
        try {
            return peopleDao.selectSome(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addPeople(People people) {
        try {
            return peopleDao.addPeople(people);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deletePeople(String pno) {
        int iNo = Integer.parseInt(pno);
        try {
            return peopleDao.deletePeople(iNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteAllPeople(String[] people) {
        for (String p: people) {
            int count = deletePeople(p);
        }
        return people.length;
    }

    @Override
    public int updatePeople(People people) {
        try {
            return peopleDao.updatePeople(people);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String p_name) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<People> peopleList = peopleDao.limit(p, s, p_name);
            DataResult dataResult = new DataResult(0,"查询成功",peopleList,peopleDao.count(p_name));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
