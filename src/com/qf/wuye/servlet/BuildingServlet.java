package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.Building;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.service.BuildingService;
import com.qf.wuye.service.impl.BuildingServiceImpl;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/27 20:11
 * 我无敌，你随意
 */
@WebServlet("/building/*")
@MultipartConfig
public class BuildingServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String b_name = request.getParameter("b_name");
        BuildingService buildingService = new BuildingServiceImpl();
        DataResult d = buildingService.limit(page, limit, b_name);
        return d;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        //传参
        String info = request.getParameter("info");
        //转换成java对象
        ObjectMapper mapper = new ObjectMapper();
        Building building = mapper.readValue(info, Building.class);
        //调用service方法
        BuildingService buildingService = new BuildingServiceImpl();
        int count = buildingService.addBuilding(building);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String id = request.getParameter("bno");
        BuildingService buildingService = new BuildingServiceImpl();
        int i = buildingService.deleteBuilding(id);
        DataResult dataResult;
        if (i > 0){
            dataResult = new DataResult(0,"删除成功");
        }else{
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String id = request.getParameter("bno");
        BuildingService buildingService = new BuildingServiceImpl();
        Building building = buildingService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功",building);
        return dataResult;
    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String info = request.getParameter("info");
        //转换
        ObjectMapper mapper = new ObjectMapper();
        Building building = mapper.readValue(info, Building.class);
        BuildingService buildingService = new BuildingServiceImpl();
        int count = buildingService.updateBuilding(building);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] idsList = mapper.readValue(ids, String[].class);
        BuildingService buildingService = new BuildingServiceImpl();
        int i = buildingService.deleteAllbuilding(idsList);
        DataResult dataResult;
        if (i > 0 ){
            dataResult = new DataResult(0,"删除"+i+"条数据成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult alll(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        BuildingService buildingService = new BuildingServiceImpl();
        List<Building> buildingList = buildingService.selectAll();
        DataResult dataResult = new DataResult(0,"查询成功",buildingList);
        return dataResult;
    }

    public DataResult include(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String cno = request.getParameter("cno");
        BuildingService buildingService = new BuildingServiceImpl();
        List<Building> buildingList = buildingService.selectSome(cno);
        DataResult dataResult = new DataResult(0,"查询成功",buildingList);
        return dataResult;
    }
}
