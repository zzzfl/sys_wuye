package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.User;
import com.qf.wuye.entity.Warranty;
import com.qf.wuye.service.UserService;
import com.qf.wuye.service.WarrantyService;
import com.qf.wuye.service.impl.UserServiceImpl;
import com.qf.wuye.service.impl.WarrantyServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

/**
 * 时间：2021/6/22 19:22
 * 我无敌，你随意
 */
@WebServlet("/user/*")
public class UserServlet extends BaseServlet{

    public DataResult login(HttpServletRequest request, HttpServletResponse response){
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String code = request.getParameter("code");
        //调用service层方法
        UserService userService = new UserServiceImpl();
        User user = new User(username,password);
        boolean flag = userService.selectUserByName(user);
        DataResult dataResult;
        if(flag){
            HttpSession session = request.getSession();
            String text = (String) session.getAttribute("text");
            if(code.equalsIgnoreCase(text)){
                dataResult = new DataResult(0,"登录成功",user);
                session.setAttribute("loginUser",user);
            }else {
                dataResult = new DataResult(40000,"验证码错误");
            }
        }else{
            dataResult = new DataResult(40000,"用户名和密码错误");
        }
        HttpSession session = request.getSession();
        session.removeAttribute("text");
        return dataResult;
    }

    public DataResult all(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String username = request.getParameter("username");
        UserService userService = new UserServiceImpl();
        DataResult d = userService.limit(page, limit, username);
        return d;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        User user = mapper.readValue(info, User.class);
        UserService userService = new UserServiceImpl();
        int count = userService.addUser(user);
        DataResult dataResult ;
        if (count > 0){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String id = request.getParameter("id");
        UserService userService = new UserServiceImpl();
        int count = userService.delUser(id);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] userList = mapper.readValue(ids, String[].class);
        UserService userService = new UserServiceImpl();
        int count = userService.delAll(userList);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"成功删除"+count+"条数据");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String id = request.getParameter("id");
        UserService userService = new UserServiceImpl();
        User user = userService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功",user);
        return dataResult;

    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        User user = mapper.readValue(info, User.class);
        UserService userService = new UserServiceImpl();
        int count = userService.updateUser(user);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }
}
