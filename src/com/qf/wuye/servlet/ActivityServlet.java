package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.Activity;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.service.ActivityService;
import com.qf.wuye.service.impl.ActivityServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

/**
 * 时间：2021/6/24 10:03
 * 我无敌，你随意
 */
@WebServlet("/activity/*")
public class ActivityServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String sponsor = request.getParameter("sponsor");
        ActivityService activityService = new ActivityServiceImpl();
        DataResult dataResult = activityService.limit(page, limit, sponsor);
        return dataResult;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Activity activity = mapper.readValue(info, Activity.class);
        ActivityService activityService = new ActivityServiceImpl();
        int count = activityService.addActivity(activity);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String id = request.getParameter("activity_id");
        ActivityService activityService = new ActivityServiceImpl();
        int count = activityService.deleteActivity(id);
        DataResult dataResult ;
        if (count > 0){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] activityList = mapper.readValue(ids, String[].class);
        ActivityService activityService = new ActivityServiceImpl();
        int count = activityService.deleteAll(activityList);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"成功删除"+count+"条数据");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String id = request.getParameter("activity_id");
        ActivityService activityService = new ActivityServiceImpl();
        Activity activity = activityService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功",activity);
        return dataResult;

    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Activity activity = mapper.readValue(info, Activity.class);
        ActivityService activityService = new ActivityServiceImpl();
        int count = activityService.updateActivity(activity);
        DataResult dataResult ;
        if (count > 0){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }

}
