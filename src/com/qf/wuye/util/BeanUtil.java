package com.qf.wuye.util;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 时间：2021/4/14 19:26
 * 我无敌，你随意
 */
public class BeanUtil {

    public static <T> T convertBean(Map<String,String[]> map,Class<T> c){
        try {
            T t = (T) c.newInstance();
            ConvertUtils.register(new Converter() {
                @Override
                public Object convert(Class aClass, Object o) {
                    if (o instanceof String){
                        String time = (String) o;
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            return sdf.parse(time);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                    return null;
                }
            }, Date.class);
            BeanUtils.populate(t,map);
            return t;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}
