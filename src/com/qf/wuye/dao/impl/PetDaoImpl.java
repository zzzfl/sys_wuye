package com.qf.wuye.dao.impl;

import com.qf.wuye.dao.PetDao;
import com.qf.wuye.entity.Pet;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/28 15:21
 * 我无敌，你随意
 */
public class PetDaoImpl implements PetDao {
    @Override
    public List<Pet> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from pet";
        return qr.query(sql,new BeanListHandler<Pet>(Pet.class));
    }


    @Override
    public Pet selectOne(int petno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from Pet where petno = ?";
        Object[] args = {petno};
        return qr.query(sql, new BeanHandler<Pet>(Pet.class),args);
    }

    @Override
    public int addPet(Pet pet) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into Pet values (?,?,?,?,?,?)";
        Object[] args = {pet.getPetno(),pet.getImage(),pet.getPno(),pet.getPet_name(),pet.getColor(),pet.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int deletPet(int petno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from Pet where petno = ?";
        Object[] args = {petno};
        return qr.update(sql,args);
    }

    @Override
    public int updatePet(Pet pet) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update Pet set image=?,pno=?,pet_name=?,color=?,date=? where petno=?";
        Object[] args = {pet.getImage(),pet.getPno(),pet.getPet_name(),pet.getColor(),pet.getDate(),pet.getPetno()};
        return qr.update(sql,args);
    }

    @Override
    public List<Pet> limit(Integer currentPage, Integer pageSize, String pet_name) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (pet_name == null){
            String sql = "select * from Pet limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Pet>(Pet.class),args);
        }else {
            String sql = "select * from Pet where pet_name like ? limit ?,?";
            Object[] args = {"%"+pet_name+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Pet>(Pet.class),args);
        }
    }

    @Override
    public int count(String pet_name) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (pet_name == null){
            String sql = "select count(*) from Pet";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from Pet where pet_name like ?";
            Object[] args = {"%"+pet_name+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
