package com.qf.wuye.dao.impl;

import com.qf.wuye.dao.CommunityDao;
import com.qf.wuye.entity.Community;
import com.qf.wuye.util.JDBCUtils;
import com.qf.wuye.entity.Community;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/26 9:07
 * 我无敌，你随意
 */
public class CommunityDaoImpl implements CommunityDao {
    @Override
    public List<Community> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from community";
        return qr.query(sql,new BeanListHandler<Community>(Community.class));
    }

    @Override
    public Community selectOne(int cno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from community where cno = ?";
        Object[] args = {cno};
        return qr.query(sql,new BeanHandler<Community>(Community.class),args);
    }

    @Override
    public int addCommunity(Community community) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into community values (?,?,?,?,?,?,?,?,?,?,?)";
        Object[] args = {community.getCno(),community.getC_number(),community.getC_name(),community.getAddr(),community.getArea(),community.getBuilding_sum(),community.getFamily_sum(),community.getImage(),community.getDeveloper(),community.getProperty_company(),community.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int deleteCommunity(int cno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from community where cno = ?";
        Object[] args = {cno};
        return qr.update(sql,args);
    }

    @Override
    public int updateCommunity(Community community) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update community set c_number=?,c_name=?,addr=?,area=?,building_sum=?,family_sum=?,image=?,developer=?,property_company=?,date=? where cno=?";
        Object[] args = {community.getC_number(),community.getC_name(),community.getAddr(),community.getArea(),community.getBuilding_sum(),community.getFamily_sum(),community.getImage(),community.getDeveloper(),community.getProperty_company(),community.getDate(),community.getCno()};
        return qr.update(sql,args);
    }

    @Override
    public List<Community> limit(Integer currentPage, Integer pageSize, String addr) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (addr == null) {
            String sql = "select * from community limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Community>(Community.class),args);
        }else {
            String sql = "select * from community where addr like ? limit ?,?";
            Object[] args = {"%"+addr+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Community>(Community.class),args);
        }
    }

    @Override
    public int count(String addr) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (addr == null) {
            String sql = "select count(*) from community ";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from community where addr like ?";
            Object[] args = {"%"+addr+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
