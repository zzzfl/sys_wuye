package com.qf.wuye.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 时间：2021/6/24 17:49
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Equipment {
    private Integer equipment_id;
    private Integer cno;
    private String equipment_number;
    private String equipment_name;
    private String brand;
    private Integer price;
    private Integer buy_sum;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date buy_time;
    private Integer use_time;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date date;
}
