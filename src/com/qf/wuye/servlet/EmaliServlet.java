package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.Complaint;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Email;
import com.qf.wuye.entity.Estate;
import com.qf.wuye.service.ComplaintService;
import com.qf.wuye.service.EmailService;
import com.qf.wuye.service.impl.ComplaintServiceImpl;
import com.qf.wuye.service.impl.EmailServiceImpl;
import sun.font.EAttribute;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * 时间：2021/6/24 15:39
 * 我无敌，你随意
 */
@WebServlet("/email/*")
public class EmaliServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String email_name = request.getParameter("email_name");
        EmailService emailService = new EmailServiceImpl();
        DataResult dataResult = emailService.limit(page, limit, email_name);
        return dataResult;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Email email = mapper.readValue(info, Email.class);
        EmailService emailService = new EmailServiceImpl();
        int count = emailService.addEmail(email);
        DataResult dataResult ;
        if (count > 0){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String email_id = request.getParameter("email_id");
        EmailService emailService = new EmailServiceImpl();
        int count = emailService.delEmail(email_id);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] strings = mapper.readValue(ids, String[].class);
        EmailService emailService = new EmailServiceImpl();
        int count = emailService.delAll(strings);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"成功删除"+count+"条数据");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String id = request.getParameter("email_id");
        EmailService emailService = new EmailServiceImpl();
        Email email = emailService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功",email);
        return dataResult;
    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Email email = mapper.readValue(info, Email.class);
        EmailService emailService = new EmailServiceImpl();
        int count = emailService.updateEmail(email);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }


}
