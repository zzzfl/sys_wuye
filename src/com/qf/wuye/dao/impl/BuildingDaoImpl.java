package com.qf.wuye.dao.impl;

import com.qf.wuye.dao.BuildingDao;
import com.qf.wuye.entity.Building;
import com.qf.wuye.util.JDBCUtils;
import com.qf.wuye.dao.BuildingDao;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/27 16:27
 * 我无敌，你随意
 */
public class BuildingDaoImpl implements BuildingDao {
    @Override
    public List<Building> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from building";
        return qr.query(sql,new BeanListHandler<Building>(Building.class));
    }

    @Override
    public Building selectOne(int bno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from building where bno =?";
        Object[] args = {bno};
        return qr.query(sql,new BeanHandler<Building>(Building.class),args);
    }

    @Override
    public List<Building> selectSome(int cno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from building where cno = ?";
        Object[] args = {cno};
        return qr.query(sql,new BeanListHandler<Building>(Building.class),args);
    }

    @Override
    public int addBuilding(Building building) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into building values (?,?,?,?,?,?)";
        Object[] args = {building.getBno(),building.getCno(),building.getB_number(),building.getB_name(),building.getFamily_sum(),building.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int deleteBuilding(int bno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from building where bno = ?";
        Object[] args = {bno};
        return qr.update(sql,args);
    }

    @Override
    public int updateBuilding(Building building) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update building set cno=?, b_number=?, b_name=?, family_sum=?, date=? where bno=?";
        Object[] args = {building.getCno(),building.getB_number(),building.getB_name(),building.getFamily_sum(),building.getDate(),building.getBno()};
        return qr.update(sql,args);
    }

    @Override
    public List<Building> limit(Integer currentPage, Integer pageSize, String b_name) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (b_name == null){
            String sql = "select * from building limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Building>(Building.class),args);
        }else {
            String sql = "select * from building where b_name like ? limit ?,?";
            Object[] args = {"%"+b_name+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Building>(Building.class),args);
        }
    }

    @Override
    public int count(String b_name) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (b_name == null){
            String sql = "select count(*) from building";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from building where b_name like ?";
            return qr.query(sql,new ScalarHandler<Number>(),"%"+b_name+"%").intValue();
        }
    }
}
