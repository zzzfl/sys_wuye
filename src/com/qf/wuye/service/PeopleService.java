package com.qf.wuye.service;

import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.People;

import java.util.List;

/**
 * 时间：2021/4/28 9:46
 * 我无敌，你随意
 */
public interface PeopleService {
    List<People> selectAll();
    People selectOne(String pno);
    List<People> selectSome(String cno);
    int addPeople(People people);
    int deletePeople(String pno);
    int deleteAllPeople(String[] people);
    int updatePeople(People people);

    DataResult limit(String currentPage, String pageSize, String p_name);
}
