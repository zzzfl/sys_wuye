package com.qf.wuye.dao;

import com.qf.wuye.entity.User;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/22 17:59
 * 我无敌，你随意
 */
public interface UserDao {
    //查询
    User selectByName(String username) throws Exception;
    //注册
    List<User> selectAll() throws SQLException;
    User selectOne(int id) throws SQLException;
    int addUser(User user) throws SQLException;
    int delUser(int id) throws SQLException;
    int updateUser(User user) throws SQLException;
    List<User> limit(Integer currentPage, Integer pageSize, String username) throws SQLException;
    int count(String username) throws SQLException;

}
