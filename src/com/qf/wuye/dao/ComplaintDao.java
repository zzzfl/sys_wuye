package com.qf.wuye.dao;

import com.qf.wuye.entity.Complaint;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 15:09
 * 我无敌，你随意
 */
public interface ComplaintDao {
    List<Complaint> selectAll() throws SQLException;
    Complaint selectOne(int complaint_id) throws SQLException;
    int addComplaint(Complaint complaint) throws SQLException;
    int delComplaint(int complaint_id) throws SQLException;
    int updateComplaint(Complaint complaint) throws SQLException;
    List<Complaint> limit (Integer currentPage, Integer pageSize, String thing) throws SQLException;
    int count(String thing) throws SQLException;
}
