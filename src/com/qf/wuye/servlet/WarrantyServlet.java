package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Warranty;
import com.qf.wuye.service.WarrantyService;
import com.qf.wuye.service.impl.WarrantyServiceImpl;
import sun.security.x509.IPAddressName;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * 时间：2021/6/24 14:31
 * 我无敌，你随意
 */
@WebServlet("/warranty/*")
public class WarrantyServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String brief = request.getParameter("brief");
        WarrantyService warrantyService = new WarrantyServiceImpl();
        DataResult d = warrantyService.limit(page, limit, brief);
        return d;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Warranty warranty = mapper.readValue(info, Warranty.class);
        WarrantyService warrantyService = new WarrantyServiceImpl();
        int count = warrantyService.addWarranty(warranty);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String id = request.getParameter("warranty_id");
        WarrantyService warrantyService = new WarrantyServiceImpl();
        int count = warrantyService.delWarranty(id);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] warrantyList = mapper.readValue(ids, String[].class);
        WarrantyService warrantyService = new WarrantyServiceImpl();
        int count = warrantyService.delAll(warrantyList);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"成功删除"+count+"条数据");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String id = request.getParameter("warranty_id");
        WarrantyService warrantyService = new WarrantyServiceImpl();
        Warranty warranty = warrantyService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功",warranty);
        return dataResult;

    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Warranty warranty = mapper.readValue(info, Warranty.class);
        WarrantyService warrantyService = new WarrantyServiceImpl();
        int count = warrantyService.updateWarranty(warranty);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }

}
