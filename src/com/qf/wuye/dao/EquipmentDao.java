package com.qf.wuye.dao;

import com.qf.wuye.entity.Equipment;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 17:53
 * 我无敌，你随意
 */
public interface EquipmentDao {
    List<Equipment> selectAll() throws SQLException;
    Equipment selectOne(int equipment_id) throws SQLException;
    int addEquipment(Equipment equipment) throws SQLException;
    int delEquipment(int equipment_id) throws SQLException;
    int updateEquipment(Equipment equipment) throws SQLException;
    List<Equipment> limit(Integer currentPage, Integer pageSize, String brand) throws SQLException;
    int count(String brand) throws SQLException;
}
