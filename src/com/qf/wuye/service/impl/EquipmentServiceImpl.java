package com.qf.wuye.service.impl;

import com.qf.wuye.dao.EquipmentDao;
import com.qf.wuye.dao.impl.EquipmentDaoImpl;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Equipment;
import com.qf.wuye.service.EquipmentService;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 19:38
 * 我无敌，你随意
 */
public class EquipmentServiceImpl implements EquipmentService {
    EquipmentDao equipmentDao = new EquipmentDaoImpl();

    @Override
    public List<Equipment> selectAll() {
        try {
            return equipmentDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Equipment selectOne(String equipment_id) {
        int id = Integer.parseInt(equipment_id);
        try {
            return equipmentDao.selectOne(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addEquipment(Equipment equipment) {
        try {
            return equipmentDao.addEquipment(equipment);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delEquipment(String equipment_id) {
        int id = Integer.parseInt(equipment_id);
        try {
            return equipmentDao.delEquipment(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delAll(String[] equipment_ids) {
        for (String id : equipment_ids  ) {
            int count = delEquipment(id);
        }
        return equipment_ids.length;
    }

    @Override
    public int updateEquipment(Equipment equipment) {
        try {
            return equipmentDao.updateEquipment(equipment);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String brand) {
        int p = 1;
        int s = 5;
        if (currentPage != null){
            p = Integer.parseInt(currentPage);
        }
        if (pageSize != null){
            s = Integer.parseInt(pageSize);
        }
        try {
            List<Equipment> equipmentList = equipmentDao.limit(p, s, brand);
            DataResult dataResult = new DataResult(0,"查询成功",equipmentList,equipmentDao.count(brand));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
