package com.qf.wuye.service.impl;

import com.qf.wuye.dao.UserDao;
import com.qf.wuye.dao.impl.UserDaoImpl;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.User;
import com.qf.wuye.entity.Warranty;
import com.qf.wuye.service.UserService;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/22 19:16
 * 我无敌，你随意
 */
public class UserServiceImpl implements UserService {
    UserDao userDao = new UserDaoImpl();

    @Override
    public boolean selectUserByName(User user) {
        try {
            User user1 = userDao.selectByName(user.getUsername());
            if(user1 != null){
                if(user1.getPassword().equals(user.getPassword())){
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<User> selectAll() {
        try {
            return userDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public User selectOne(String id) {
        int no = Integer.parseInt(id);
        try {
            return userDao.selectOne(no);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addUser(User user) {
        try {
            return userDao.addUser(user);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delUser(String id) {
        int no = Integer.parseInt(id);
        try {
            return userDao.delUser(no);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delAll(String[] ids) {
        for (String id : ids  ) {
            int count = delUser(id);
        }
        return ids.length;
    }

    @Override
    public int updateUser(User user) {
        try {
            return userDao.updateUser(user);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String username) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<User> userList = userDao.limit(p, s, username);
            DataResult dataResult = new DataResult(0,"查询成功",userList,userDao.count(username));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
