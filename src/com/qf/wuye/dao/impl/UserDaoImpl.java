package com.qf.wuye.dao.impl;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.dao.UserDao;
import com.qf.wuye.entity.User;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import javax.sql.rowset.JdbcRowSet;
import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/22 18:00
 * 我无敌，你随意
 */
public class UserDaoImpl implements UserDao {
    @Override
    public User selectByName(String username) throws Exception {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from user where username = ?";
        Object[] args = {username};
        return qr.query(sql,new BeanHandler<User>(User.class),args);
    }

    @Override
    public List<User> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from user";
        return qr.query(sql,new BeanListHandler<User>(User.class));
    }

    @Override
    public User selectOne(int id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from user where id = ?";
        Object[] args = {id};
        return qr.query(sql, new BeanHandler<User>(User.class),args);
    }

    @Override
    public int addUser(User user) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into user values (?,?,?,?,?,?,?)";
        Object[] args = {user.getId(),user.getUsername(),user.getPassword(),user.getPhone(),user.getEmail(),
        user.getPosition(),user.getStart_time()};
        return qr.update(sql,args);
    }

    @Override
    public int delUser(int id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from user where id = ?";
        Object[] args = {id};
        return qr.update(sql,args);
    }

    @Override
    public int updateUser(User user) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update user set username=?,password=?,phone=?,email=?,position=?,start_time=? where id=?";
        Object[] args = {user.getUsername(),user.getPassword(),user.getPhone(),user.getEmail(),
                user.getPosition(),user.getStart_time(),user.getId()};
        return qr.update(sql,args);
    }

    @Override
    public List<User> limit(Integer currentPage, Integer pageSize, String username) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (username == null){
            String sql = "select * from user limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<User>(User.class),args);
        }else {
            String sql = "select * from user where username like ? limit ?,?";
            Object[] args = {"%"+username+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<User>(User.class),args);
        }
    }

    @Override
    public int count(String username) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (username == null ){
            String sql = "select count(*) from user";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from user where username like ?";
            Object[] args = {"%"+username+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
