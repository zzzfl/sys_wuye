package com.qf.wuye.dao.impl;

import com.alibaba.druid.sql.ast.statement.SQLForeignKeyImpl;
import com.alibaba.druid.sql.visitor.functions.Char;
import com.qf.wuye.dao.ChargeDao;
import com.qf.wuye.entity.Charge;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 21:02
 * 我无敌，你随意
 */
public class ChargeDaoImpl implements ChargeDao {
    @Override
    public List<Charge> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from charge";
        return qr.query(sql,new BeanListHandler<Charge>(Charge.class));
    }

    @Override
    public Charge selectOne(int charge_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from charge where charge_id = ?";
        Object[] args = {charge_id};
        return qr.query(sql,new BeanHandler<Charge>(Charge.class),args);
    }

    @Override
    public List<Charge> selectSome(int cno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from charge where cno = ? ";
        Object[] args = {cno};
        return qr.query(sql,new BeanListHandler<Charge>(Charge.class),args);
    }

    @Override
    public int addCharge(Charge charge) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into charge values(?,?,?,?,?)";
        Object[] args = {charge.getCharge_id(),charge.getCno(),charge.getCharge_number(),
        charge.getCharge_name(),charge.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int delCharge(int charge_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from charge where charge_id = ?";
        Object[] args = {charge_id};
        return qr.update(sql,args);
    }

    @Override
    public int updateCharge(Charge charge) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update charge set cno=?,charge_number=?,charge_name=?,date=? where charge_id=?";
        Object[] args = {charge.getCno(),charge.getCharge_number(),
                charge.getCharge_name(),charge.getDate(),charge.getCharge_id()};
        return qr.update(sql,args);
    }

    @Override
    public List<Charge> limit(Integer currentPage, Integer pageSize, String charge_name) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (charge_name == null){
            String sql = "select * from charge limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Charge>(Charge.class),args);
        }else {
            String sql = "select * from charge where charge_name like ? limit ?,?";
            Object[] args = {"%"+charge_name+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Charge>(Charge.class),args);
        }
    }

    @Override
    public int count(String charge_name) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (charge_name == null){
            String sql = "select count(*) from charge";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from charge where charge_name like ?";
            Object[] args = {"%"+charge_name+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
