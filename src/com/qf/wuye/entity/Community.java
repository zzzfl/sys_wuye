package com.qf.wuye.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 时间：2021/4/25 17:26
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Community {
    private Integer cno;
    private String c_number;
    private String c_name;
    private String addr;
    private Integer area;
    private Integer building_sum;
    private Integer family_sum;
    private String image;   //缩略图
    private String developer;
    private String property_company;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date date;
    @JsonIgnore
    private String file;

    public Community(String c_name) {
        this.c_name = c_name;
    }
}
