package com.qf.wuye.dao.impl;

import com.qf.wuye.dao.EstateDao;
import com.qf.wuye.entity.Estate;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/26 16:51
 * 我无敌，你随意
 */
public class EstateDaoImpl implements EstateDao {
    @Override
    public List<Estate> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from estate";
        return qr.query(sql,new BeanListHandler<Estate>(Estate.class));
    }

    @Override
    public Estate selectOne(int eno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from estate where eno = ?";
        Object[] args = {eno};
        return qr.query(sql,new BeanHandler<Estate>(Estate.class),args);
    }

    @Override
    public int addEstate(Estate estate) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into estate values (?,?,?,?,?,?,?,?)";
        Object[] args = {estate.getEno(),estate.getCno(),estate.getBno(),estate.getDoor_number(),estate.getLandlord(),estate.getPhone(),estate.getRoom_sum(),estate.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int deleteEstate(int eno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from estate where eno = ?";
        Object[] args = {eno};
        return qr.update(sql,args);
    }

    @Override
    public int updateEstate(Estate estate) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update estate set cno=?,bno=?,door_number=?,landlord=?,phone=?,room_sum=?,date=? where eno=?";
        Object[] args = {estate.getCno(),estate.getBno(),estate.getDoor_number(),estate.getLandlord(),estate.getPhone(),estate.getRoom_sum(),estate.getDate(),estate.getEno()};
        return qr.update(sql,args);
    }

    @Override
    public List<Estate> limit(Integer currentPage, Integer pageSize, String landlord) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (landlord == null ){
            String sql = "select * from estate limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Estate>(Estate.class),args);
        }else {
            String sql = "select * from estate where landlord like ? limit ?,?";
            Object[] args = {"%"+landlord+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Estate>(Estate.class),args);
        }
    }

    @Override
    public int count(String landlord) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (landlord == null){
            String sql = "select count(*) from estate";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from estate where landlord like ?";
            return qr.query(sql,new ScalarHandler<Number>(),"%"+landlord+"%").intValue();
        }
    }
}
