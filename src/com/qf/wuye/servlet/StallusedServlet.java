package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Stallused;
import com.qf.wuye.service.StallService;
import com.qf.wuye.service.StallusedService;
import com.qf.wuye.service.impl.StallServiceImpl;
import com.qf.wuye.service.impl.StallusedServiceImpl;
import com.qf.wuye.util.FileUploadUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * 时间：2021/6/23 16:07
 * 我无敌，你随意
 */
@WebServlet("/stallused/*")
@MultipartConfig
public class StallusedServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String stall_number = request.getParameter("stall_number");
        StallusedService stallusedService = new StallusedServiceImpl();
        DataResult dataResult = stallusedService.limit(page, limit, stall_number);
        return dataResult;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Stallused stallused = mapper.readValue(info,Stallused.class);
        StallusedService stallusedService = new StallusedServiceImpl();
        int count = stallusedService.addStallused(stallused);
        DataResult dataResult;
        if(count > 0 ){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String id = request.getParameter("stallused_id");
        StallusedService stallusedService = new StallusedServiceImpl();
        int count = stallusedService.deleteStallused(id);
        DataResult dataResult;
        if(count > 0){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] stallusedlist = mapper.readValue(ids, String[].class);
        StallusedService stallusedService = new StallusedServiceImpl();
        int count = stallusedService.deleteAllStallused(stallusedlist);
        DataResult dataResult;
        if(count > 0){
            dataResult = new DataResult(0,"成功删除"+count+"条数据");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String id = request.getParameter("stallused_id");
        StallusedService stallusedService = new StallusedServiceImpl();
        Stallused stallused = stallusedService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功",stallused);
        return dataResult;
    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Stallused stallused = mapper.readValue(info, Stallused.class);
        StallusedService stallusedService = new StallusedServiceImpl();
        int count = stallusedService.updateStallused(stallused);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }

    public DataResult upload(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        DataResult dataResult;
        try {
            String sqlPath = FileUploadUtil.upload(request);
            dataResult = new DataResult(0,"上传成功",sqlPath);
        } catch (Exception e) {
            e.printStackTrace();
            dataResult = new DataResult(40000,"上传失败");
        }
        return dataResult;
    }



}
