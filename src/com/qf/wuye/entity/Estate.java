package com.qf.wuye.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 时间：2021/4/25 17:40
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Estate {
    private Integer eno;
    private Integer cno;   //房子所属小区id
    private Integer bno;    //房子所属栋
    private Integer door_number;
    private String landlord;     //户主名字
    private String phone;        //户主电话
    private Integer room_sum;       //房间数
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date date;          //购买时间
    @JsonIgnore
    private String file;
    private Community community;

}
