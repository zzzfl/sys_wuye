package com.qf.wuye.dao;

import com.qf.wuye.entity.Stall;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/28 16:15
 * 我无敌，你随意
 */
public interface StallDao {
    List<Stall> selectAll() throws SQLException;
    Stall selectOne(int stallno) throws SQLException;
    int addStall(Stall stall) throws SQLException;
    int deleteStall(int stallno) throws SQLException;
    int updateStall(Stall stall) throws SQLException;

    List<Stall> limit(Integer currentPage, Integer pageSize, String stall_number) throws SQLException;
    int count(String stall_number) throws SQLException;
}
