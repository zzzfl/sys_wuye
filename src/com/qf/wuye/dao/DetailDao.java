package com.qf.wuye.dao;

import com.qf.wuye.entity.Detail;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/25 9:16
 * 我无敌，你随意
 */
public interface DetailDao {
    List<Detail> selectAll() throws SQLException;
    Detail selectOne(int detail_id) throws SQLException;
    int addDetail(Detail detail) throws SQLException;
    int delDetail(int detail_id) throws SQLException;
    int updateDetail(Detail detail) throws SQLException;
    List<Detail> limit(Integer currentPage, Integer pageSize, String brief) throws SQLException;
    int count(String brief) throws SQLException;
}
