package com.qf.wuye.service.impl;

import com.qf.wuye.dao.DetailDao;
import com.qf.wuye.dao.impl.DetailDaoImpl;
import com.qf.wuye.entity.Complaint;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Detail;
import com.qf.wuye.service.DetailService;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/25 9:33
 * 我无敌，你随意
 */
public class DetailServiceImpl implements DetailService {
    DetailDao detailDao = new DetailDaoImpl();

    @Override
    public List<Detail> selectAll() {
        try {
            return detailDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Detail selectOne(String detail_id) {
        int id = Integer.parseInt(detail_id);
        try {
            return detailDao.selectOne(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addDetail(Detail detail) {
        try {
            return detailDao.addDetail(detail);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delDetail(String detail_id) {
        int id = Integer.parseInt(detail_id);
        try {
            return detailDao.delDetail(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delAll(String[] detail_ids) {
        for (String id : detail_ids      ) {
            int count = delDetail(id);
        }
        return detail_ids.length;
    }

    @Override
    public int updateDetail(Detail detail) {
        try {
            return detailDao.updateDetail(detail);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String brief) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null ){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<Detail> detailList = detailDao.limit(p, s, brief);
            DataResult dataResult = new DataResult(0,"查询成功",detailList,detailDao.count(brief));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
