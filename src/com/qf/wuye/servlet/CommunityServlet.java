package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.Community;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.service.CommunityService;
import com.qf.wuye.service.impl.CommunityServiceImpl;
import com.qf.wuye.util.ExcelUtils;
import com.qf.wuye.util.FileUploadUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 时间：2021/4/26 9:26
 * 我无敌，你随意
 */
@WebServlet("/community/*")
@MultipartConfig
public class CommunityServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String addr = request.getParameter("addr");
        CommunityService communityService = new CommunityServiceImpl();
        DataResult dataResult = communityService.limit(page,limit,addr);
        return dataResult;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Community community = mapper.readValue(info,Community.class);
        CommunityService communityService = new CommunityServiceImpl();
        int count =communityService.addCommunity(community);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String id = request.getParameter("cno");
        CommunityService communityService = new CommunityServiceImpl();
        int count = communityService.deleteCommunity(id);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] idArr = mapper.readValue(ids, String[].class);
        CommunityService communityService = new CommunityServiceImpl();
        int count = communityService.deleteAllCommunity(idArr);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"成功删除"+count+"条数据");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String id = request.getParameter("cno");
        CommunityService communityService = new CommunityServiceImpl();
        Community community = communityService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功",community);
        return dataResult;
    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Community community = mapper.readValue(info, Community.class);
        CommunityService communityService = new CommunityServiceImpl();
        int count = communityService.updateCommunity(community);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }

    public DataResult upload(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        DataResult dataResult;
        try {
            String sqlPath = FileUploadUtil.upload(request);
            dataResult = new DataResult(0,"上传成功",sqlPath);
        } catch (Exception e) {
            e.printStackTrace();
            dataResult = new DataResult(40000,"上传失败");
        }
        return dataResult;
    }

    public DataResult alll(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        CommunityService communityService = new CommunityServiceImpl();
        List<Community> communityList = communityService.selectAll();
        DataResult dataResult = new DataResult(0,"查询成功",communityList);
        return dataResult;
    }

    public DataResult uploadExcel(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        //1.根据上传的文件获取文件的输入流对象
        Part part = request.getPart("file");
        InputStream ins = part.getInputStream();
        //获取上传文件的后缀名
        String fileName = part.getSubmittedFileName();
        String fileExt = fileName.substring(fileName.lastIndexOf(".")+1);
        //3.调用工具类中的导入方法
        List<String[]> data = ExcelUtils.importData(fileExt, ins, 1);
        //4.调用service层方法,将数据插入到数据库
        CommunityService communityService = new CommunityServiceImpl();
        int count = communityService.addAllCommunity(data);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"成功添加"+count+"条数据");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;

    }



    public void downloadExcel(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1.获取所有的community数据
        CommunityService communityService = new CommunityServiceImpl();
        List<Community> communityList = communityService.selectAll();

        //设置响应头，告知浏览器是下载操作
        response.setHeader("Content-DisPosition","attachment;filename=table.xls");
        //设置MIME类型
        response.setContentType("application/vnd.ms-excel");

        //2.将communityList转成Excel表格
        //定义表格头
        String[] titles = {"小区id","小区编号","小区名字","所在城市","占地面积","总栋数","总户数","小区缩略图","开发商","物业管理公司","创建时间"};
        //将communityList转换成list<String[]>的数据格式
        ExcelUtils.export(titles,toArray(communityList),response.getOutputStream());
    }

    public List<String[]> toArray(List<Community> communityList){
        List<String[]> data = new ArrayList<>();
        for (Community community : communityList ) {
            String[] row = new String[11];
            row[0] = String.valueOf(community.getCno());
            row[1] = community.getC_number();
            row[2] = community.getC_name();
            row[3] = community.getAddr();
            row[4] = String.valueOf(community.getArea());
            row[5] = String.valueOf(community.getBuilding_sum());
            row[6] = String.valueOf(community.getFamily_sum());
            row[7] = community.getImage();
            row[8] = community.getDeveloper();
            row[9] = community.getProperty_company();
            row[10] = new SimpleDateFormat("yyyy-MM-dd").format(community.getDate());
            data.add(row);
        }
        return data;
    }
}
