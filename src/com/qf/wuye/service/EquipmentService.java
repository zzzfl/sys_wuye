package com.qf.wuye.service;

import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Equipment;

import java.util.List;

/**
 * 时间：2021/6/24 19:30
 * 我无敌，你随意
 */
public interface EquipmentService {
    List<Equipment> selectAll();
    Equipment selectOne(String equipment_id);
    int addEquipment(Equipment equipment);
    int delEquipment(String equipment_id);
    int delAll(String[] equipment_ids);
    int updateEquipment(Equipment equipment);
    DataResult limit(String currentPage, String pageSize, String brand);
}
