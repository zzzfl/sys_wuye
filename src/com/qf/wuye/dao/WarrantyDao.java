package com.qf.wuye.dao;

import com.qf.wuye.entity.Warranty;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 14:02
 * 我无敌，你随意
 */
public interface WarrantyDao {
    List<Warranty> selectAll() throws SQLException;
    Warranty selectOne(int warranty_id) throws SQLException;
    int addWarranty(Warranty warranty) throws SQLException;
    int deleteWarranty(int warranty_id) throws SQLException;
    int updateWarranty(Warranty warranty) throws SQLException;
    List<Warranty> limit(Integer currentPage, Integer pageSize, String brief) throws SQLException;
    int count(String brief) throws SQLException;

}
