package com.qf.wuye.dao;

import com.qf.wuye.entity.Community;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/26 9:02
 * 我无敌，你随意
 */
public interface CommunityDao {
    //查询所有
    List<Community> selectAll() throws SQLException;
    Community selectOne(int cno) throws SQLException;
    int addCommunity(Community community) throws SQLException;
    int deleteCommunity(int cno) throws SQLException;
    int updateCommunity(Community community) throws SQLException;
    List<Community>limit(Integer currentPage, Integer pageSize, String addr) throws SQLException;

    int count(String addr) throws SQLException;
}
