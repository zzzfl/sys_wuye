package com.qf.wuye.service;

import com.qf.wuye.entity.Activity;
import com.qf.wuye.entity.DataResult;

import java.util.List;

/**
 * 时间：2021/6/24 9:49
 * 我无敌，你随意
 */
public interface ActivityService {
    List<Activity> selectAll();
    Activity selectOne(String activity_id);
    int addActivity(Activity activity);
    int deleteActivity(String activity_id);
    int deleteAll(String[] activity_ids);
    int updateActivity(Activity activity);

    DataResult limit(String currentPage, String pageSize, String sponsor);
}
