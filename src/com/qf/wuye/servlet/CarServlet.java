package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.Car;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.service.CarService;
import com.qf.wuye.service.impl.CarServiceImpl;
import com.qf.wuye.util.FileUploadUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * 时间：2021/4/28 14:28
 * 我无敌，你随意
 */
@WebServlet("/car/*")
@MultipartConfig
public class CarServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String car_number = request.getParameter("car_number");
        CarService carService = new CarServiceImpl();
        DataResult dataResult = carService.limit(page, limit, car_number);
        return dataResult;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Car car = mapper.readValue(info, Car.class);
        CarService carService = new CarServiceImpl();
        int count = carService.addCar(car);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String id = request.getParameter("carno");
        CarService carService = new CarServiceImpl();
        int count = carService.deleteCar(id);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] carIdList = mapper.readValue(ids, String[].class);
        CarService carService = new CarServiceImpl();
        int count = carService.deleteAllCar(carIdList);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"成功删除"+count+"条数据");
        }else{
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String id = request.getParameter("carno");
        CarService carService = new CarServiceImpl();
        Car car = carService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功", car);
        return dataResult;

    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Car car = mapper.readValue(info, Car.class);
        CarService carService = new CarServiceImpl();
        int count = carService.updateCar(car);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }

    public DataResult upload(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException{
        DataResult dataResult;
        try {
            String sqlPath = FileUploadUtil.upload(request);
            dataResult = new DataResult(0,"上传成功",sqlPath);
            return dataResult;
        } catch (ServletException e) {
            e.printStackTrace();
            dataResult = new DataResult(40000,"上传失败");
        }
        return dataResult;
    }


}
