package com.qf.wuye.service.impl;

import com.qf.wuye.dao.ComplaintDao;
import com.qf.wuye.dao.impl.ComplaintDaoImpl;
import com.qf.wuye.entity.Complaint;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.service.ComplaintService;
import com.sun.corba.se.spi.ior.iiop.IIOPFactories;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 15:33
 * 我无敌，你随意
 */
public class ComplaintServiceImpl implements ComplaintService {
    ComplaintDao complaintDao = new ComplaintDaoImpl();

    @Override
    public List<Complaint> selectAll() {
        try {
            return complaintDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Complaint selectOne(String complaint_id) {
        int id = Integer.parseInt(complaint_id);
        try {
            return complaintDao.selectOne(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addComplaint(Complaint complaint) {
        try {
            return complaintDao.addComplaint(complaint);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delComplaint(String complaint_id) {
        int id = Integer.parseInt(complaint_id);
        try {
            return complaintDao.delComplaint(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delAll(String[] complaint_ids) {
        for ( String id : complaint_ids     ) {
            int count = delComplaint(id);
        }
        return complaint_ids.length;
    }

    @Override
    public int updateComplaint(Complaint complaint) {
        try {
            return complaintDao.updateComplaint(complaint);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String thing) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null ){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<Complaint> complaintList = complaintDao.limit(p, s, thing);
            DataResult dataResult = new DataResult(0,"查询成功",complaintList,complaintDao.count(thing));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
