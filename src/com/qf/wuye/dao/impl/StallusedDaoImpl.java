package com.qf.wuye.dao.impl;

import com.qf.wuye.dao.StallusedDao;
import com.qf.wuye.entity.Stall;
import com.qf.wuye.entity.Stallused;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/23 14:58
 * 我无敌，你随意
 */
public class StallusedDaoImpl implements StallusedDao {
    @Override
    public List<Stallused> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from stallused";
        return qr.query(sql, new BeanListHandler<Stallused>(Stallused.class));

    }

    @Override
    public Stallused selectOne(int stallused_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from stallused where stallused_id = ?";
        Object[] args = {stallused_id};
        return qr.query(sql,new BeanHandler<Stallused>(Stallused.class),args);
    }

    @Override
    public int addStallused(Stallused stallused) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into stallused values (?,?,?,?,?,?,?,?,?,?,?)";
        Object[] args = {stallused.getStallused_id(),stallused.getCno(),stallused.getStall_number(),
                stallused.getCar_number(),stallused.getImage(),stallused.getPno(),
                stallused.getPhone(),stallused.getPrice(),stallused.getStartime(),
                stallused.getEndtime(),stallused.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int deleteStallused(int stallused_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from stallused where stallused_id = ?";
        Object[] args = {stallused_id};
        return qr.update(sql,args);
    }

    @Override
    public int updateStallused(Stallused stallused) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update stallused set cno=?,stall_number=?,car_number=?,image=?,pno=?,phone=?,price=?,startime=?,endtime=?,date=? where stallused_id=?";
        Object[] args = {stallused.getCno(),stallused.getStall_number(),
                stallused.getCar_number(),stallused.getImage(),stallused.getPno(),
                stallused.getPhone(),stallused.getPrice(),stallused.getStartime(),
                stallused.getEndtime(),stallused.getDate(),stallused.getStallused_id()};
        return qr.update(sql,args);
    }

    @Override
    public List<Stallused> limit(Integer currentPage, Integer pageSize, String stall_number) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if(stall_number == null){
            String sql = "select * from stallused limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Stallused>(Stallused.class),args);
        }else {
            String sql = "select * from stallused where stall_number like ? limit ?,?";
            Object[] args = {"%"+stall_number+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Stallused>(Stallused.class),args);
        }
    }

    @Override
    public int count(String stall_number) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if(stall_number == null){
            String sql = "select count(*) from stallused";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from stallused where stall_number like ?";
            Object[] args = {"%"+stall_number+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
