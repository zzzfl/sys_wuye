package com.qf.wuye.dao;

import com.qf.wuye.entity.Building;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/27 16:10
 * 我无敌，你随意
 */
public interface BuildingDao {
    //查询所有
    List<Building> selectAll() throws SQLException;
    //查询单个
    Building selectOne(int bno) throws SQLException;
    //查询一些
    List<Building> selectSome(int cno) throws SQLException;
    //增加
    int addBuilding(Building building) throws SQLException;
    //删除
    int deleteBuilding(int bno) throws SQLException;
    //修改
    int updateBuilding(Building building) throws SQLException;
    //分页查询
    List<Building> limit(Integer currentPage,Integer pageSize,String b_name) throws SQLException;

    int count(String b_name) throws SQLException;
}
