package com.qf.wuye.service;

import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Stall;

import java.util.List;

/**
 * 时间：2021/4/28 16:35
 * 我无敌，你随意
 */
public interface StallService {
    List<Stall> selectAll();
    Stall selectOne(String stallno);
    int addStall(Stall stall);
    int deleteStall(String stallno);
    int deleteAllStall(String[] stalls);
    int updateStall(Stall stall);

    DataResult limit(String currentPage, String pageSize, String stall_number);
}
