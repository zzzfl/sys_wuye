package com.qf.wuye.service;

import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Email;

import java.util.List;

/**
 * 时间：2021/6/24 16:44
 * 我无敌，你随意
 */
public interface EmailService {
    List<Email> selectAll();
    Email selectOne(String email_id);
    int addEmail(Email email);
    int delEmail(String email_id);
    int delAll(String[] email_ids);
    int updateEmail(Email email);
    DataResult limit(String currentPage, String pageSize, String email_name);

}
