package com.qf.wuye.service;

import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Warranty;

import java.util.List;

/**
 * 时间：2021/6/24 14:20
 * 我无敌，你随意
 */
public interface WarrantyService {
    List<Warranty> selectAll();
    Warranty selectOne(String warranty_id);
    int addWarranty(Warranty warranty);
    int delWarranty(String warranty_id);
    int delAll(String[] warranty_ids);
    int updateWarranty(Warranty warranty);
    DataResult limit(String currentPage, String pageSize, String brief);
}
