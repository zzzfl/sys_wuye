package com.qf.wuye.service.impl;

import com.qf.wuye.dao.CommunityDao;
import com.qf.wuye.dao.impl.CommunityDaoImpl;
import com.qf.wuye.entity.Community;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.service.CommunityService;
import com.qf.wuye.entity.Community;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.service.CommunityService;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * 时间：2021/4/26 9:21
 * 我无敌，你随意
 */
public class CommunityServiceImpl implements CommunityService {
    CommunityDao communityDao = new CommunityDaoImpl();
    @Override
    public List<Community> selectAll() {
        try {
            return communityDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Community selectOne(String cno) {
        try {
            int iNo = Integer.parseInt(cno);
            return communityDao.selectOne(iNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addCommunity(Community community) {
        try {
            return communityDao.addCommunity(community);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteCommunity(String cno) {
        try {
            int cNo =Integer.parseInt(cno);
            return communityDao.deleteCommunity(cNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteAllCommunity(String[] communities) {
        for (String community : communities ) {
            int count = deleteCommunity(community);
        }
        return communities.length;
    }

    @Override
    public int updateCommunity(Community community) {
        try {
            return communityDao.updateCommunity(community);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int addAllCommunity(List<String[]> data) {
        //1、直接循环调用添加单个的方法(但是要考虑事务问题。解决方案：动态拼接Sql语句)
        // insert into emp (,,,,) values(,,,,),(,,,,),(,,,,)
        if (data == null || data.size() == 0){
            return 0;
        }
        for (String[] row : data){
            Community community = new Community();
            community.setCno(Integer.parseInt(row[0]));
            community.setC_number(row[1]);
            community.setC_name(row[2]);
            community.setAddr(row[3]);
            community.setArea(Integer.parseInt(row[4]));
            community.setBuilding_sum(Integer.parseInt(row[5]));
            community.setFamily_sum(Integer.parseInt(row[6]));
            community.setImage(row[7]);
            community.setDeveloper(row[8]);
            community.setProperty_company(row[9]);
            try {
                community.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(row[10]));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            addCommunity(community);
        }
        return data.size();
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String addr) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<Community> communityList = communityDao.limit(p, s,addr);
            DataResult dataResult = new DataResult(0,"查询成功",communityList,communityDao.count(addr));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
