package com.qf.wuye.dao.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.dao.EmailDao;
import com.qf.wuye.entity.Email;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;
import sun.font.EAttribute;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 16:31
 * 我无敌，你随意
 */
public class EmailDaoImpl implements EmailDao {
    @Override
    public List<Email> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from email";
        return qr.query(sql,new BeanListHandler<Email>(Email.class));
    }

    @Override
    public Email selectOne(int email_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from email where email_id = ?";
        Object[] args = {email_id};
        return qr.query(sql,new BeanHandler<Email>(Email.class),args);
    }

    @Override
    public int addEmail(Email email) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into email values (?,?,?,?,?)";
        Object[] args = {email.getEmail_id(),email.getCno(),email.getPno(),email.getEmail_name(),email.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int delEmail(int email_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from email where email_id = ?";
        Object[] args = {email_id};
        return qr.update(sql,args);
    }

    @Override
    public int updateEmail(Email email) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update email set cno=?, pno=?, email_name=?, date=? where email_id=?";
        Object[] args = {email.getCno(),email.getPno(),email.getEmail_name(),email.getDate(),email.getEmail_id()};
        return qr.update(sql,args);
    }

    @Override
    public List<Email> limit(Integer currentPage, Integer pageSize, String email_name) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (email_name == null){
            String sql = "select * from email limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Email>(Email.class),args);
        }else {
            String sql = "select * from email where email_name like ? limit ?,?";
            Object[] args = {"%"+email_name+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Email>(Email.class),args);
        }
    }

    @Override
    public int count(String email_name) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (email_name == null){
            String sql = "select count(*) from email";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from email where email_name like ?";
            Object[] args = {"%"+email_name+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
