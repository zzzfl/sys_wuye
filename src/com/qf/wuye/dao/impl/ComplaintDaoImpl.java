package com.qf.wuye.dao.impl;

import com.alibaba.druid.sql.ast.statement.SQLForeignKeyImpl;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.dao.ComplaintDao;
import com.qf.wuye.entity.Complaint;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * 时间：2021/6/24 15:12
 * 我无敌，你随意
 */
public class ComplaintDaoImpl implements ComplaintDao {

    @Override
    public List<Complaint> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from complaint";
        return qr.query(sql, new BeanListHandler<Complaint>(Complaint.class));
    }

    @Override
    public Complaint selectOne(int complaint_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from complaint where complaint_id = ?";
        Object[] args = {complaint_id};
        return qr.query(sql,new BeanHandler<Complaint>(Complaint.class),args);

    }

    @Override
    public int addComplaint(Complaint complaint) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into complaint values (?,?,?,?,?,?)";
        Object[] args = {complaint.getComplaint_id(),complaint.getCno(),complaint.getPno(),
        complaint.getThing(),complaint.getReason(),complaint.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int delComplaint(int complaint_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from complaint where complaint_id = ?";
        Object[] args = {complaint_id};
        return qr.update(sql,args);
    }

    @Override
    public int updateComplaint(Complaint complaint) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update complaint set cno=?,pno=?,thing=?,reason=?,date=? where complaint_id=?";
        Object[] args = {complaint.getCno(),complaint.getPno(),
                complaint.getThing(),complaint.getReason(),complaint.getDate(),complaint.getComplaint_id()};
        return qr.update(sql,args);
    }

    @Override
    public List<Complaint> limit(Integer currentPage, Integer pageSize, String thing) throws SQLException {
     QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
     if (thing == null ){
         String sql = "select * from complaint limit ?,?";
         Object[] args = {(currentPage-1)*pageSize,pageSize};
         return qr.query(sql,new BeanListHandler<Complaint>(Complaint.class),args);
     }else {
         String sql = "select * from complaint where thing like ? limit ?,?";
         Object[] args = {"%"+thing+"%",(currentPage-1)*pageSize,pageSize};
         return qr.query(sql, new BeanListHandler<Complaint>(Complaint.class),args);
     }
    }

    @Override
    public int count(String thing) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (thing == null){
            String sql = "select count(*) from complaint";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from complaint where thing like ?";
            Object[] args = {"%"+thing+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
