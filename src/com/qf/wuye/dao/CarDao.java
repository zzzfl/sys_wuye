package com.qf.wuye.dao;

import com.qf.wuye.entity.Car;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/28 12:01
 * 我无敌，你随意
 */
public interface CarDao {
    List<Car> selectAll() throws SQLException;
    Car selectOne(int carno) throws SQLException;
    int addCar(Car car) throws SQLException;
    int deletCar(int carno) throws SQLException;
    int updateCar(Car car) throws SQLException;

    List<Car> limit(Integer currentPage, Integer pageSize, String car_number) throws SQLException;
    int count(String car_number) throws SQLException;

}
