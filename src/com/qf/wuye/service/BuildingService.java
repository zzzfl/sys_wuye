package com.qf.wuye.service;

import com.qf.wuye.entity.Building;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Building;
import com.qf.wuye.entity.DataResult;

import java.util.List;

/**
 * 时间：2021/4/27 16:55
 * 我无敌，你随意
 */
public interface BuildingService {
    List<Building> selectAll();//查所有
    Building selectOne(String bno);//查单个
    List<Building> selectSome(String cno);
    int addBuilding(Building building);//
    int deleteBuilding(String bno);
    int deleteAllbuilding(String[] bnos);
    int updateBuilding(Building building);

    DataResult limit(String currentPage, String pageSize , String b_name);
}
