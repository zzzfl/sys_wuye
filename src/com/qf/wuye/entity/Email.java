package com.qf.wuye.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 时间：2021/6/24 16:25
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Email {
    private Integer email_id;
    private Integer cno;
    private Integer pno;
    private String email_name;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date date;
    @JsonIgnore
    private String file;

}
