package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.dao.ChargeDao;
import com.qf.wuye.entity.*;
import com.qf.wuye.service.ActivityService;
import com.qf.wuye.service.BuildingService;
import com.qf.wuye.service.ChargeService;
import com.qf.wuye.service.PeopleService;
import com.qf.wuye.service.impl.ActivityServiceImpl;
import com.qf.wuye.service.impl.BuildingServiceImpl;
import com.qf.wuye.service.impl.ChargeServiceImpl;
import com.qf.wuye.service.impl.PeopleServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 10:03
 * 我无敌，你随意
 */
@WebServlet("/charge/*")
public class ChargeServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String charge_name = request.getParameter("charge_name");
        ChargeService chargeService = new ChargeServiceImpl();
        DataResult dataResult = chargeService.limit(page, limit, charge_name);
        return dataResult;
    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Charge charge = mapper.readValue(info, Charge.class);
        ChargeService chargeService = new ChargeServiceImpl();
        int count = chargeService.addCharge(charge);
        DataResult dataResult;
        if (count > 0 ){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String id = request.getParameter("charge_id");
        ChargeService chargeService = new ChargeServiceImpl();
        int count = chargeService.delCharge(id);
        DataResult dataResult ;
        if (count > 0){
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] chargeList = mapper.readValue(ids, String[].class);
        ChargeService chargeService = new ChargeServiceImpl();
        int count = chargeService.delAll(chargeList);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"成功删除"+count+"条数据");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String id = request.getParameter("charge_id");
        ChargeService chargeService = new ChargeServiceImpl();
        Charge charge = chargeService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功",charge);
        return dataResult;

    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response)throws SQLException, IOException {
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        Charge charge = mapper.readValue(info, Charge.class);
        ChargeService chargeService = new ChargeServiceImpl();
        int count = chargeService.updateCharge(charge);
        DataResult dataResult ;
        if (count > 0){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }

    public DataResult alll(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        ChargeService chargeService = new ChargeServiceImpl();
        List<Charge> chargeList = chargeService.selectAll();
        DataResult dataResult = new DataResult(0,"查询成功",chargeList);
        return dataResult;
    }

    public DataResult include(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String cno = request.getParameter("cno");
        ChargeService chargeService = new ChargeServiceImpl();
        List<Charge> chargeList = chargeService.selectSome(cno);
        DataResult dataResult = new DataResult(0,"查询成功",chargeList);
        return dataResult;
    }

}
