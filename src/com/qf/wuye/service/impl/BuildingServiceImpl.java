package com.qf.wuye.service.impl;

import com.qf.wuye.dao.BuildingDao;
import com.qf.wuye.dao.impl.BuildingDaoImpl;
import com.qf.wuye.entity.Building;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.service.BuildingService;
import com.qf.wuye.dao.BuildingDao;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/27 16:59
 * 我无敌，你随意
 */
public class BuildingServiceImpl implements BuildingService {
    BuildingDao buildingDao = new BuildingDaoImpl();
    @Override
    public List<Building> selectAll() {
        try {
            return buildingDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Building selectOne(String bno) {
        try {
            int iNo = Integer.parseInt(bno);
            return buildingDao.selectOne(iNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Building> selectSome(String cno) {
        try {
            int iNo = Integer.parseInt(cno);
            return buildingDao.selectSome(iNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addBuilding(Building building) {
        try {
            return buildingDao.addBuilding(building);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteBuilding(String bno) {
        int iNo = Integer.parseInt(bno);
        try {
            return buildingDao.deleteBuilding(iNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteAllbuilding(String[] bnos) {
        for (String id : bnos ) {
            int count = deleteBuilding(id);
        }
        return bnos.length;
    }

    @Override
    public int updateBuilding(Building building) {
        try {
            return buildingDao.updateBuilding(building);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String b_name) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<Building> buildingList = buildingDao.limit(p, s, b_name);
            DataResult dataResult = new DataResult(0,"查询成功",buildingList,buildingDao.count(b_name));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口失败");
        return dataResult;
    }
}
