package com.qf.wuye.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 时间：2021/6/25 9:12
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Detail {
    private Integer detail_id;
    private Integer cno;
    private Integer charge_id;
    private Integer pno;
    private Integer should;
    private Integer fact;
    private String brief;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date pay_time;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date date;
}
