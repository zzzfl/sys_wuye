package com.qf.wuye.dao.impl;

import com.alibaba.druid.stat.JdbcSqlStatValue;
import com.qf.wuye.dao.DetailDao;
import com.qf.wuye.entity.Detail;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/25 9:19
 * 我无敌，你随意
 */
public class DetailDaoImpl implements DetailDao {
    @Override
    public List<Detail> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from detail";
        return qr.query(sql,new BeanListHandler<Detail>(Detail.class));
    }

    @Override
    public Detail selectOne(int detail_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from detail where detail_id = ?";
        Object[] args = {detail_id};
        return qr.query(sql,new BeanHandler<Detail>(Detail.class),args);
    }

    @Override
    public int addDetail(Detail detail) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into detail values (?,?,?,?,?,?,?,?,?)";
        Object[] args = {detail.getDetail_id(),detail.getCno(),detail.getCharge_id(),detail.getPno(),
        detail.getShould(),detail.getFact(),detail.getBrief(),detail.getPay_time(),detail.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int delDetail(int detail_id) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from detail where detail_id = ?";
        Object[] args = {detail_id};
        return qr.update(sql,args);
    }

    @Override
    public int updateDetail(Detail detail) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update detail set cno=?,charge_id=?,pno=?,should=?,fact=?,brief=?,pay_time=?,date=? where detail_id=?";
        Object[] args = {detail.getCno(),detail.getCharge_id(),detail.getPno(),detail.getShould(),
                detail.getFact(),detail.getBrief(),detail.getPay_time(),detail.getDate(),detail.getDetail_id()};
        return qr.update(sql,args);
    }

    @Override
    public List<Detail> limit(Integer currentPage, Integer pageSize, String brief) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (brief == null){
            String sql = "select * from detail limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Detail>(Detail.class),args);
        }else {
            String sql = "select * from detail where brief like ? limit ?,?";
            Object[] args = {"%"+brief+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Detail>(Detail.class),args);
        }
    }

    @Override
    public int count(String brief) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (brief == null){
            String sql = "select count(*) from detail";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from detail where brief like ?";
            Object[] args = {"%"+brief+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
