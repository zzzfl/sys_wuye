package com.qf.wuye.service.impl;

import com.qf.wuye.dao.StallDao;
import com.qf.wuye.dao.impl.StallDaoImpl;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Stall;
import com.qf.wuye.service.StallService;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/28 16:37
 * 我无敌，你随意
 */
public class StallServiceImpl implements StallService {
    StallDao stallDao = new StallDaoImpl();
    @Override
    public List<Stall> selectAll() {
        try {
            return stallDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Stall selectOne(String stallno) {
        try {
            int iNo = Integer.parseInt(stallno);
            return stallDao.selectOne(iNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addStall(Stall stall) {
        try {
            return stallDao.addStall(stall);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteStall(String stallno) {
        int iNo = Integer.parseInt(stallno);
        try {
            return stallDao.deleteStall(iNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteAllStall(String[] stalls) {
        for (String stall : stalls) {
            int count = deleteStall(stall);
        }
        return stalls.length;
    }

    @Override
    public int updateStall(Stall stall) {
        try {
            return stallDao.updateStall(stall);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String stall_number) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<Stall> stallList = stallDao.limit(p, s, stall_number);
            DataResult dataResult = new DataResult(0,"查询成功",stallList,stallDao.count(stall_number));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
