package com.qf.wuye.service.impl;

import com.qf.wuye.dao.ChargeDao;
import com.qf.wuye.dao.impl.ChargeDaoImpl;
import com.qf.wuye.entity.Car;
import com.qf.wuye.entity.Charge;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.service.ChargeService;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/24 23:39
 * 我无敌，你随意
 */
public class ChargeServiceImpl implements ChargeService {
    ChargeDao chargeDao = new ChargeDaoImpl();

    @Override
    public List<Charge> selectAll() {
        try {
            return chargeDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Charge selectOne(String charge_id) {
        int id = Integer.parseInt(charge_id);
        try {
            return chargeDao.selectOne(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public List<Charge> selectSome(String cno) {
        int id = Integer.parseInt(cno);
        try {
            return chargeDao.selectSome(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addCharge(Charge charge) {
        try {
            return chargeDao.addCharge(charge);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delCharge(String charge_id) {
        int id = Integer.parseInt(charge_id);
        try {
            return chargeDao.delCharge(id);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int delAll(String[] charge_ids) {
        for (String id : charge_ids  ) {
            int count = delCharge(id);
        }
        return charge_ids.length;
    }

    @Override
    public int updateCharge(Charge charge) {
        try {
            return chargeDao.updateCharge(charge);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String charge_name) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<Charge> chargeList = chargeDao.limit(p, s, charge_name);
            DataResult dataResult = new DataResult(0,"查询成功",chargeList,chargeDao.count(charge_name));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
