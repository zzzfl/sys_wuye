package com.qf.wuye.service;

import com.qf.wuye.entity.Complaint;
import com.qf.wuye.entity.DataResult;

import java.util.Date;
import java.util.List;

/**
 * 时间：2021/6/24 15:30
 * 我无敌，你随意
 */
public interface ComplaintService {
    List<Complaint> selectAll();
    Complaint selectOne(String complaint_id);
    int addComplaint(Complaint complaint);
    int delComplaint(String complaint_id);
    int delAll(String[] complaint_ids);
    int updateComplaint(Complaint complaint);
    DataResult limit(String currentPage, String pageSize, String thing);
}
