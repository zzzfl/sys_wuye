package com.qf.wuye.dao;

import com.qf.wuye.entity.Estate;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/26 16:41
 * 我无敌，你随意
 */
public interface EstateDao {
    //查询所有
    List<Estate> selectAll() throws SQLException;
    //查询单个
    Estate selectOne(int eno) throws SQLException;
    //增加
    int addEstate(Estate estate) throws SQLException;
    //删除
    int deleteEstate(int eno) throws SQLException;
    //修改
    int updateEstate(Estate estate) throws SQLException;
    //分页查询
    List<Estate> limit(Integer currentPage,Integer pageSize,String landlord) throws SQLException;

    int count(String landlord) throws SQLException;
}
