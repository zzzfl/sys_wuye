package com.qf.wuye.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 时间：2021/6/24 9:07
 * 我无敌，你随意
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Activity {
    private Integer activity_id;
    private Integer cno;
    private String activity_name;
    private String place;
    private String sponsor;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date startime;
    @JsonFormat(pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date endtime;
    @JsonFormat( pattern = "yyyy-MM-dd",timezone = "GMT+8")
    private Date date;
    @JsonIgnore
    private String file;
}
