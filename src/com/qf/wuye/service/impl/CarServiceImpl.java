package com.qf.wuye.service.impl;

import com.qf.wuye.dao.CarDao;
import com.qf.wuye.dao.impl.CarDaoImpl;
import com.qf.wuye.entity.Car;
import com.qf.wuye.entity.Car;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.service.CarService;
import com.qf.wuye.entity.DataResult;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/28 14:19
 * 我无敌，你随意
 */
public class CarServiceImpl implements CarService {
    CarDao carDao = new CarDaoImpl();
    @Override
    public List<Car> selectAll() {
        try {
            return carDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Car selectOne(String carno) {
        int iNo = Integer.parseInt(carno);
        try {
            return carDao.selectOne(iNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addCar(Car car) {
        try {
            return carDao.addCar(car);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteCar(String carno) {
        int iNo = Integer.parseInt(carno);
        try {
            return carDao.deletCar(iNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteAllCar(String[] cars) {
        for (String car : cars) {
            int count = deleteCar(car);
        }
        return cars.length;
    }

    @Override
    public int updateCar(Car car) {
        try {
            return carDao.updateCar(car);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String car_number) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<Car> carList = carDao.limit(p, s, car_number);
            DataResult dataResult = new DataResult(0,"查询成功",carList, carDao.count(car_number));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
