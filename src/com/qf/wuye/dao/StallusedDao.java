package com.qf.wuye.dao;

import com.qf.wuye.entity.Stallused;

import javax.rmi.CORBA.Stub;
import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/6/23 14:48
 * 我无敌，你随意
 */
public interface StallusedDao {
    List<Stallused> selectAll() throws SQLException;
    Stallused selectOne(int stallused_id) throws SQLException;
    int addStallused(Stallused stallused) throws SQLException;
    int deleteStallused(int stallused_id) throws SQLException;
    int updateStallused(Stallused stallused) throws SQLException;

    List<Stallused> limit(Integer currentPage, Integer pageSize, String stall_number) throws SQLException;
    int count(String stall_number) throws SQLException;
}
