package com.qf.wuye.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 时间：2021/4/16 15:18
 * 我无敌，你随意
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageBean<T>{
    private Integer pages;      //总页数
    private Integer totalCount;       //总条数
    private Integer currentPage;        //当前页
    private Integer pageSize;       //每页显示条数
    private List<T> data;        //每页数据


}
