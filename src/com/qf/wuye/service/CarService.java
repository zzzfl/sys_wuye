package com.qf.wuye.service;

import com.qf.wuye.entity.Car;
import com.qf.wuye.entity.DataResult;

import java.util.List;

/**
 * 时间：2021/4/28 14:16
 * 我无敌，你随意
 */
public interface CarService {
    List<Car> selectAll();
    Car selectOne(String carno);
    int addCar(Car car);
    int deleteCar(String carno);
    int deleteAllCar(String[] cars);
    int updateCar(Car car);

    DataResult limit(String currentPage, String pageSize, String car_number);
}
