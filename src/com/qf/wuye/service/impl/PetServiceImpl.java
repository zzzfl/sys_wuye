package com.qf.wuye.service.impl;

import com.qf.wuye.dao.PetDao;
import com.qf.wuye.dao.impl.PetDaoImpl;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Pet;
import com.qf.wuye.service.PetService;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/28 15:30
 * 我无敌，你随意
 */
public class PetServiceImpl implements PetService {
    PetDao petDao = new PetDaoImpl();
    @Override
    public List<Pet> selectAll() {
        try {
            return petDao.selectAll();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public Pet selectOne(String petno) {
        int iNo = Integer.parseInt(petno);
        try {
            return petDao.selectOne(iNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    @Override
    public int addPet(Pet pet) {
        try {
            return petDao.addPet(pet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deletePet(String petno) {
        int iNo = Integer.parseInt(petno);
        try {
            return petDao.deletPet(iNo);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public int deleteAllPet(String[] pets) {
        for (String pet : pets) {
            int count = deletePet(pet);
        }
        return pets.length;
    }

    @Override
    public int updatePet(Pet pet) {
        try {
            return petDao.updatePet(pet);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return 0;
    }

    @Override
    public DataResult limit(String currentPage, String pageSize, String pet_name) {
        try {
            int p = 1;
            int s = 5;
            if (currentPage != null){
                p = Integer.parseInt(currentPage);
            }
            if (pageSize != null){
                s = Integer.parseInt(pageSize);
            }
            List<Pet> petList = petDao.limit(p, s, pet_name);
            DataResult dataResult = new DataResult(0,"查询成功",petList, petDao.count(pet_name));
            return dataResult;
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        DataResult dataResult = new DataResult(40000,"接口错误");
        return dataResult;
    }
}
