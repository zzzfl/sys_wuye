package com.qf.wuye.service;

import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Pet;

import java.util.List;

/**
 * 时间：2021/4/28 15:29
 * 我无敌，你随意
 */
public interface PetService {
    List<Pet> selectAll();
    Pet selectOne(String petno);
    int addPet(Pet pet);
    int deletePet(String petno);
    int deleteAllPet(String[] cars);
    int updatePet(Pet pet);

    DataResult limit(String currentPage, String pageSize, String pet_name);
}
