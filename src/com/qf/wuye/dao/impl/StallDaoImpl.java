package com.qf.wuye.dao.impl;

import com.qf.wuye.dao.StallDao;
import com.qf.wuye.entity.Stall;
import com.qf.wuye.util.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/28 16:22
 * 我无敌，你随意
 */
public class StallDaoImpl implements StallDao {
    @Override
    public List<Stall> selectAll() throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from stall";
        return qr.query(sql,new BeanListHandler<Stall>(Stall.class));
    }

    @Override
    public Stall selectOne(int stallno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "select * from stall where stallno = ?";
        Object[] args = {stallno};
        return qr.query(sql,new BeanHandler<Stall>(Stall.class),args);
    }

    @Override
    public int addStall(Stall stall) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "insert into stall values (?,?,?,?)";
        Object[] args = {stall.getStallno(),stall.getCno(),stall.getStall_number(),stall.getDate()};
        return qr.update(sql,args);
    }

    @Override
    public int deleteStall(int stallno) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "delete from stall where stallno = ?";
        Object[] args = {stallno};
        return qr.update(sql,args);
    }

    @Override
    public int updateStall(Stall stall) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        String sql = "update stall set cno=?,stall_number=?,date=? where stallno=?";
        Object[] args = {stall.getCno(),stall.getStall_number(),stall.getDate(),stall.getStallno()};
        return qr.update(sql,args);
    }

    @Override
    public List<Stall> limit(Integer currentPage, Integer pageSize, String stall_number) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (stall_number == null){
            String sql = "select * from stall limit ?,?";
            Object[] args = {(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Stall>(Stall.class),args);
        }else {
            String sql = "select * from stall where stall_number like ? limit ?,?";
            Object[] args = {"%"+stall_number+"%",(currentPage-1)*pageSize,pageSize};
            return qr.query(sql,new BeanListHandler<Stall>(Stall.class),args);
        }
    }

    @Override
    public int count(String stall_number) throws SQLException {
        QueryRunner qr = new QueryRunner(JDBCUtils.getDataSource());
        if (stall_number == null){
            String sql = "select count(*) from stall";
            return qr.query(sql,new ScalarHandler<Number>()).intValue();
        }else {
            String sql = "select count(*) from stall where stall_number like ?";
            Object[] args = {"%"+stall_number+"%"};
            return qr.query(sql,new ScalarHandler<Number>(),args).intValue();
        }
    }
}
