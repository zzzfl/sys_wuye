package com.qf.wuye.service;

import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.Estate;

import java.util.List;

/**
 * 时间：2021/4/26 17:19
 * 我无敌，你随意
 */
public interface EstateService {
    List<Estate> selectAll();//查所有
    Estate selectOne(String eno);//查单个
    int deleteAllEstate(String[] estates);//批量删除
    int deleteEstate(String eno);//删除单个
    int addEstate(Estate estate);//添加
    int updateEstate(Estate estate);//修改
    DataResult limit(String currentPage,String pageSize,String landlord);//分页搜索
}
