package com.qf.wuye.dao;

import com.qf.wuye.entity.People;

import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/28 9:18
 * 我无敌，你随意
 */
public interface PeopleDao {
    List<People> selectAll() throws SQLException;
    People selectOne(int pno) throws SQLException;
    List<People> selectSome(int cno) throws SQLException;
    int addPeople(People people) throws SQLException;
    int deletePeople(int pno) throws SQLException;
    int updatePeople(People people) throws SQLException;

    List<People> limit(Integer currentPage, Integer pageSize, String p_name) throws SQLException;
    int count(String p_name) throws SQLException;
}
