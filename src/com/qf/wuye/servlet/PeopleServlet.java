package com.qf.wuye.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.wuye.entity.Building;
import com.qf.wuye.entity.DataResult;
import com.qf.wuye.entity.People;
import com.qf.wuye.service.BuildingService;
import com.qf.wuye.service.PeopleService;
import com.qf.wuye.service.impl.BuildingServiceImpl;
import com.qf.wuye.service.impl.PeopleServiceImpl;
import com.qf.wuye.util.FileUploadUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * 时间：2021/4/28 10:45
 * 我无敌，你随意
 */
@WebServlet("/people/*")
@MultipartConfig
public class PeopleServlet extends BaseServlet{

    public DataResult all(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String page = request.getParameter("page");
        String limit = request.getParameter("limit");
        String p_name = request.getParameter("p_name");
        PeopleService peopleService = new PeopleServiceImpl();
        DataResult dataResult = peopleService.limit(page, limit, p_name);
        return dataResult;

    }

    public DataResult add(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String info = request.getParameter("info");
        //转换
        ObjectMapper mapper = new ObjectMapper();
        People people = mapper.readValue(info, People.class);
        PeopleService peopleService = new PeopleServiceImpl();
        int count = peopleService.addPeople(people);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"添加成功");
        }else {
            dataResult = new DataResult(40000,"添加失败");
        }
        return dataResult;
    }

    public DataResult del(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String id = request.getParameter("pno");
        PeopleService peopleService = new PeopleServiceImpl();
        int count = peopleService.deletePeople(id);
        DataResult dataResult;
        if (count > 0) {
            dataResult = new DataResult(0,"删除成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult delAll(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String ids = request.getParameter("ids");
        ObjectMapper mapper = new ObjectMapper();
        String[] peopleList = mapper.readValue(ids, String[].class);
        PeopleService peopleService = new PeopleServiceImpl();
        int count = peopleService.deleteAllPeople(peopleList);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"删除"+count+"条数据成功");
        }else {
            dataResult = new DataResult(40000,"删除失败");
        }
        return dataResult;
    }

    public DataResult one(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String id = request.getParameter("pno");
        PeopleService peopleService = new PeopleServiceImpl();
        People people = peopleService.selectOne(id);
        DataResult dataResult = new DataResult(0,"查询成功",people);
        return dataResult;
    }

    public DataResult update(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String info = request.getParameter("info");
        ObjectMapper mapper = new ObjectMapper();
        People people = mapper.readValue(info, People.class);
        PeopleService peopleService = new PeopleServiceImpl();
        int count = peopleService.updatePeople(people);
        DataResult dataResult;
        if (count > 0){
            dataResult = new DataResult(0,"修改成功");
        }else {
            dataResult = new DataResult(40000,"修改失败");
        }
        return dataResult;
    }

    public DataResult upload(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        DataResult dataResult;
        try {
            String sqlPath = FileUploadUtil.upload(request);
            dataResult = new DataResult(0,"上传成功",sqlPath);
        } catch (ServletException e) {
            e.printStackTrace();
            dataResult = new DataResult(40000,"上传失败");
        }
        return dataResult;
    }

    public DataResult alll(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        PeopleService peopleService = new PeopleServiceImpl();
        List<People> peopleList = peopleService.selectAll();
        DataResult dataResult = new DataResult(0,"查询成功",peopleList);
        return dataResult;
    }

    public DataResult include(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException{
        String cno = request.getParameter("cno");
        PeopleService peopleService = new PeopleServiceImpl();
        List<People> peopleList = peopleService.selectSome(cno);
        DataResult dataResult = new DataResult(0,"查询成功",peopleList);
        return dataResult;
    }
}
